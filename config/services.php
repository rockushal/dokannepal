<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
    'client_id' => env('FB_KEY','2103238386584054'),         // Your facbook Client ID
    'client_secret' => env('FB_SECRET','83ec4fea9b045d030176c12c5ff267bd'), // Your facbook 
    'redirect' => env('FB_CALLBACK','https://localhost/dokannepal/auth/facebook/callback'),
    ],

    'google' => [
        'client_id' =>'253297489168-t7tar0kujc0l5mtid2vdassql6qjomja.apps.googleusercontent.com',
        'client_secret' => 'xTi2SA58UpPnELNHtLmYpiWK',
        'redirect' => 'http://dokannepal.com/auth/google/callback',
    ],

    'twitter' => [
        'client_id' =>'qTksVsc7m1Q1NvFnHeCUX2XY0',
        'client_secret' => 'TvAiXRirUm8SKwF9uSMFES5xVSPig02ITlauwnwcef5D2KLk2E',
        'redirect' => 'http://dokannepal.com/auth/twitter/callback',
    ],

];
