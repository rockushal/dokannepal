<!-- FOOTER -->
<footer>
  <div class="container">
    <div class="footer-top">
      <div class="row">
        <div class="col-md-12">
          <div class="footer-widget-heading">
            <h6 class="h6 light">Brands In Around Nepal</h6>
          </div>
          <div class="empty-space col-xs-b20"></div>
          <div class="footer-column-links">
            <div class="row">
              <div class="col-xs-3">
                <a href="#">Samsung</a>
                <a href="#">Nike</a>
                <a href="#">Reebok</a>
                <a href="#">Addidas</a>
                <a href="#">Nokia</a>
                <a href="#">CG</a>
                <a href="#">Dell</a>
              </div>
              <div class="col-xs-3">
                <a href="#">Samsung</a>
                <a href="#">Nike</a>
                <a href="#">Reebok</a>
                <a href="#">Addidas</a>
                <a href="#">Nokia</a>
                <a href="#">CG</a>
                <a href="#">Dell</a>
              </div>
              <div class="col-xs-3">
                <a href="#">Samsung</a>
                <a href="#">Nike</a>
                <a href="#">Reebok</a>
                <a href="#">Addidas</a>
                <a href="#">Nokia</a>
                <a href="#">CG</a>
                <a href="#">Dell</a>
              </div>
              <div class="col-xs-3">
                <a href="#">Samsung</a>
                <a href="#">Nike</a>
                <a href="#">Reebok</a>
                <a href="#">Addidas</a>
                <a href="#">Nokia</a>
                <a href="#">CG</a>
                <a href="#">Dell</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="row">
        <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
          <div class="copyright">&copy; DokanNepal 2018 All rights reserved.</div>
          <div class="follow">
            <a class="entry" href="#"><i class="fa fa-facebook"></i></a>
            <a class="entry" href="#"><i class="fa fa-twitter"></i></a>
            <a class="entry" href="#"><i class="fa fa-linkedin"></i></a>
            <a class="entry" href="#"><i class="fa fa-google-plus"></i></a>
            <a class="entry" href="#"><i class="fa fa-pinterest-p"></i></a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-text-center col-lg-text-right">
          <div class="footer-payment-icons">
            <a class="entry"><img src="img/thumbnail-4.jpg" alt="" /></a>
            <a class="entry"><img src="img/thumbnail-5.jpg" alt="" /></a>
            <a class="entry"><img src="img/thumbnail-6.jpg" alt="" /></a>
            <a class="entry"><img src="img/thumbnail-7.jpg" alt="" /></a>
            <a class="entry"><img src="img/thumbnail-8.jpg" alt="" /></a>
            <a class="entry"><img src="img/thumbnail-9.jpg" alt="" /></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
</div>

<div class="popup-wrapper">
  <div class="bg-layer"></div>

  <div class="popup-content" data-rel="1">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
      <div class="popup-align">
        <h3 class="h3 text-center">Log In</h3>
        <div class="empty-space col-xs-b30"></div>
        <form method="POST" action="{{route('user.login')}}">
          {{ csrf_field() }}
          <input class="simple-input" type="text" name="email" ="" placeholder="Your email"  required autofocus/>
          @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
          @endif
          <div class="empty-space col-xs-b10 col-sm-b20"></div>
          <input class="simple-input" type="password" name="password" placeholder="Enter password" required/>
          @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
          @endif
          <div class="empty-space col-xs-b10 col-sm-b20"></div>
          <div class="row">
            <div class="col-sm-6 col-xs-b10 col-sm-b0">
              <div class="empty-space col-sm-b5"></div>
              <a class="simple-link open-popup" data-rel="3">Forgot password?</a>
              <div class="empty-space col-xs-b5"></div>
              <a class="simple-link open-popup" data-rel="2">register now</a>
            </div>
            <div class="col-sm-6 text-right">
              <button type="submit" class="btn btn-link button size-2 style-3">
                <span class="button-wrapper">
                  <span class="icon"><img src="{{URL::to('public/front/img/icon-4.png')}}" alt="" /></span>
                  <span class="text">submit</span>
                </span>
              </button>
            </div>
          </div>
        </form>
        <div class="popup-or">
          <span>or</span>
        </div>
        <div class="row m5">
          <div class="col-sm-6 col-xs-b10 col-sm-b0">
            <a class="button twitter-button size-2 style-4 block" href="{{ url('/auth/twitter') }}">
              <span class="button-wrapper">
                <span class="icon"><img src="{{URL::to('public/front/img/icon-4.png')}}" alt="" /></span>
                <span class="text">twitter</span>
              </span>
            </a>
          </div>
          <div class="col-sm-6">
            <a class="button google-button size-2 style-4 block" href="{{ url('/auth/google') }}">
              <span class="button-wrapper">
                <span class="icon"><img src="{{URL::to('public/front/img/icon-4.png')}}" alt="" /></span>
                <span class="text">google+</span>
              </span>
            </a>
          </div>
        </div>
      </div>
      <div class="button-close"></div>
    </div>
  </div>

  <div class="popup-content" data-rel="2">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
      <div class="popup-align">
        <h3 class="h3 text-center">Register With Dokan</h3>
        <form method="post" action="{{route('userregister')}}">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="empty-space col-xs-b30"></div>
          <input class="simple-input" type="text" value="" placeholder="Your name" name="name" />
          <div class="empty-space col-xs-b10 col-sm-b20"></div>
          <input class="simple-input" type="text" value="" placeholder="Your email" name="email" />
          <div class="empty-space col-xs-b10 col-sm-b20"></div>
          <input class="simple-input" type="password" value="" placeholder="Enter password" name="password" />
          <div class="empty-space col-xs-b10 col-sm-b20"></div>
          <input class="simple-input" type="password" value="" placeholder="Repeat password" name="confirm" />
          <div class="empty-space col-xs-b10 col-sm-b20"></div>
          <div class="row">
            <div class="col-sm-7 col-xs-b10 col-sm-b0">
              <div class="empty-space col-sm-b15"></div>
              <label class="checkbox-entry">
                <input type="checkbox" /><span><a href="#">Privacy policy agreement</a></span>
              </label>
            </div>
            <div class="col-sm-5 text-right">
              <button class="btn btn-link button size-2 style-3" type="submit">
                <span class="button-wrapper">
                  <span class="icon"><img src="{{URL::to('public/front/img/icon-4.png')}}" alt="" /></span>
                  <span class="text">submit</span>
                </span>
              </button>
            </div>
          </div>
        </form>
        <div class="popup-or">
          <span>or</span>
        </div>
        <div class="row m5">
          <div class="col-sm-6 col-xs-b10 col-sm-b0">
            <a class="button twitter-button size-2 style-4 block" href="{{ url('/auth/twitter') }}">
              <span class="button-wrapper">
                <span class="icon"><img src="{{URL::to('public/front/img/icon-4.png')}}" alt="" /></span>
                <span class="text">twitter</span>
              </span>
            </a>
          </div>
          <div class="col-sm-6">
            <a class="button google-button size-2 style-4 block" href="{{ url('/auth/google') }}">
              <span class="button-wrapper">
                <span class="icon"><img src="{{URL::to('public/front/img/icon-4.png')}}" alt="" /></span>
                <span class="text">google+</span>
              </span>
            </a>
          </div>
        </div>
      </div>
      <div class="button-close"></div>
    </div>
  </div>

  <div class="popup-content" data-rel="3">
    <div class="layer-close"></div>
    <div class="popup-container size-1">
      <div class="popup-align">
        <h3 class="h3 text-center">Reset Password</h3>
        <form method="post" action="">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="empty-space col-xs-b30"></div>
          <
          <input class="simple-input" id="email" type="email" placeholder="Your email" name="email" />
          <div class="empty-space col-xs-b10 col-sm-b20"></div>
          <div class="row">
            <div class="col-sm-7 col-xs-b10 col-sm-b0">
              <div class="empty-space col-sm-b15"></div>
            </div>
            <div class="col-sm-5 text-right">
              <button class="btn btn-link button size-2 style-3" type="submit">
                <span class="button-wrapper">
                  <span class="icon"><img src="{{URL::to('public/front/img/icon-4.png')}}" alt="" /></span>
                  <span class="text">submit</span>
                </span>
              </button>
            </div>
          </div>
        </form>
      </div>
      <div class="button-close"></div>
    </div>
  </div>
</div>
<script type="text/javascript">
window.setTimeout(function() {
  $(".alert").fadeTo(500, 0).slideUp(500, function(){
    $(this).remove();
  });
}, 4000);
</script>
<script src="{{URL::to('public/front/js/jquery-2.2.4.min.js')}}"></script>
<script src="{{URL::to('public/front/js/swiper.jquery.min.js')}}"></script>
<script src="{{URL::to('public/front/js/global.js')}}"></script>

<!-- styled select -->
<script src="{{URL::to('public/front/js/jquery.sumoselect.min.js')}}"></script>

<!-- counter -->
<script src="{{URL::to('public/front/js/jquery.classycountdown.js')}}"></script>
<script src="{{URL::to('public/front/js/jquery.knob.js')}}"></script>
<script src="{{URL::to('public/front/js/jquery.throttle.js')}}"></script>
<script src="{{URL::to('public/front/js/nprogress.js')}}"></script>

</body>
</html>
