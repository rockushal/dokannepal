<!DOCTYPE html>
<html lang="en">
<head>

	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
	<meta property="og:image" content="{{URL::to('public/images/logo/'.$settings->logo)}}" />
	<title>{{$settings->title}}</title>
	<!-- fonts -->
	<link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">
	<link href="{{URL::to('public/front/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{URL::to('public/front/css/bootstrap.extension.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{URL::to('public/front/css/style.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{URL::to('public/front/css/swiper.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{URL::to('public/front/css/sumoselect.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{URL::to('public/front/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="{{URL::to('public/front/css/nprogress.css')}}">


	<link rel="stylesheet" href="{{URL::to('public/front/css/app.css')}}">

	<link rel="shortcut icon" href="{{URL::to('public/images/icon/'.$settings->favicon)}}" />

</head>
<body>
	<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.0&appId=2103238386584054&autoLogAppEvents=1';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
	<!-- LOADER -->
	<!-- <div id="loader-wrapper"></div> -->

	<div id="content-block">
		<!-- HEADER -->
		<header>
			<div class="header-top">
				<div class="content-margins">
					<div class="row">
						<div class="col-md-5 hidden-xs hidden-sm">
							<div class="entry"><b>contact us:</b> <a href="tel:+9861154717">{{$settings->phone}}</a></div>
							<div class="entry"><b>email:</b> <a href="mailto:{{$settings->email}}">{{$settings->email}}</a></div>
						</div>
						<div class="col-md-7 col-md-text-right">
							@if(Auth::guard('customer')->check())
							<div class="entry hidden-xs hidden-sm cart">
								<a href="{{route('cart',Auth::guard('customer')->user()->id)}}">
									<b class="hidden-xs">Your bag</b>
									<span class="cart-icon">
										<i class="fa fa-shopping-bag" aria-hidden="true"></i>
										<span class="cart-label">
											{{Auth::guard('customer')->user()->cart->count()}}
										</span>
									</span>
								</a>
							</div>
							<div class="entry"><a  href="{{ route('user.logout') }}">
								<i class="fa fa-fw fa-sign-out"></i><b>{{Auth::guard('customer')->user()->name}}</b></a></div>
								@else
								<div class="entry"><a class="open-popup" data-rel="1"><b>Login</b></a>&nbsp; or &nbsp;<a class="open-popup" data-rel="2"><b>Register</b></a> </div>
								@endif
								<div class="entry"><a href="{{ route('help')}}"><b><i class="fa fa-info-circle" aria-hidden="true"></i> Help</b></a></div>
								<div class="hamburger-icon">
									<span></span>
									<span></span>
									<span></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header-bottom">
					<div class="content-margins">
						<div class="row">
							<div class="col-xs-3 col-sm-1">
								<a id="logo" href="{{URL::to('/')}}"><img src="{{URL::to('public/images/logo/'.$settings->logo)}}" alt="logo" /></a>
							</div>
							<div class="col-xs-9 col-sm-11 text-right">
								<div class="nav-wrapper">
									<div class="nav-close-layer"></div>
									<nav>
										<ul>
											<li>
												<a href="{{route('listproduct')}}">Shop</a>
											</li>
											<li>
												<a href="{{route('about')}}">About Us</a>
											</li>
											<li><a href="{{route('contact')}}">Contact Us</a></li>
											@if(Auth::guard('customer')->check())
											<li><a href="{{route('profile')}}">Profile</a></li>
											<li>
												<a href="{{route('wish',Auth::guard('customer')->user()->id)}}">Wish List</a>
											</li>
											@endif
										</ul>
										<div class="navigation-title">
											Navigation
											<div class="hamburger-icon active">
												<span></span>
												<span></span>
												<span></span>
											</div>
										</div>
									</nav>
								</div>
								<div class="header-bottom-icon toggle-search"><i class="fa fa-search" aria-hidden="true"></i></div>
								@if(Auth::guard('customer')->check())
								<div class="header-bottom-icon visible-rd">
									<i class="fa fa-shopping-bag" aria-hidden="true"></i>
									<span class="cart-label">{{Auth::guard('customer')->user()->cart->count()}}
									</span>
								</div>
								@endif
							</div>
						</div>
						<div class="header-search-wrapper">
							<div class="header-search-content">
								<div class="container-fluid">
									<div class="row">
										<div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
											<form method="post" action="{{route('search')}}">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<div class="search-submit">
													<i class="fa fa-search" aria-hidden="true"></i>
													<input type="submit"/>
												</div>
												<input class="simple-input style-1" type="text" name="value" value="" placeholder="Enter keyword" />
											</form>
										</div>
									</div>
								</div>
								<div class="button-close"></div>
							</div>
						</div>
					</div>
				</div>
			</header>
			<div class="header-empty-space"></div>
			@if(Session::has('success'))
			<div class="alert alert-dismissable alert-success" style="position: fixed; z-index: 10003; bottom: 30px; right: 40px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>{{Session('success')}}</strong></div>
			@endif
			@if(Session::has('error'))
			<div class="alert alert-dismissable alert-danger" style="position: fixed; z-index: 10003; bottom: 30px; right: 40px;"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><strong>{{Session('error')}}</strong></div>
			@endif
