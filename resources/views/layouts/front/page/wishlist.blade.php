@extends('layouts.front.app')
@section('content')
<div class="cart-view-wrap">
  <div class="container">
    <!-- categories -->
      <h1 style="margin: 20px 0;font-size:  20px;font-weight: 800;">My Wish List</h1>

    <div class="category-group">
      <table class="cart-table">
        <thead>
          <tr>
            <th style="width: 195px;"></th>
            <th>product name</th>
            <th style="width: 150px;">price</th>
            <th style="width: 310px;">Product Image</th>
            <th style="width: 70px;"></th>
          </tr>
        </thead>
        <tbody>
          <?php $total=0; $item=0;?>
          @foreach($wishdetail as $key=>$wish)
          <tr>
            <td data-title=" ">
              {{$key+1}}</a>
            </td>
            <td data-title=" "><h6 class="h6"><a href="#">
              {{$wish->product->name}} </a></h6></td>
              <td data-title="Price: ">Nrs {{$wish->product->price}}</td>
              <td data-title="Price: "><img src="{{URL::to('public/images/product/'.$wish->product->image)}}" class="img-responsive" width="50%"></td>
              <td data-title="">
                <a href="{{route('deletewish',$wish->id)}}"><div class="button-close"></div></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @endsection
