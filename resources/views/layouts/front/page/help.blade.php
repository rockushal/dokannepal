@extends('layouts.front.app')
@section('content')
<div class="container">
  <div class="bread-crumbs-wrap">
    <div class="breadcrumbs">
      <a href="#">home</a>
      <a href="#">Help</a>
    </div>
  </div>
<div class="row">
    <div class="col-md-12">
     <h2 style="font-size: 25px; margin-bottom: 10px; "><b>Help</b></h2><br>   
</div>
<div class="col-md-12">
	<div class="help-page">
		<h3 style="font-size: 20px; margin-left: 20px;"><b>Login And Registration</b></h3><br><br>
	</div>
</div>
<div class="col-md-offset-3 col-md-6">
		<p>1. For registration you have to click on the register button</p><br>
		<img src="{{asset('public/front/img/help/2.jpg')}}" class="img-responsive" width="100%"><br>
			</div>
			<div class="col-md-offset-3 col-md-6">
		<p>2. After clicking on the register the form will be pop up</p><br>
		<img src="{{asset('public/front/img/help/4.jpg')}}" class="img-responsive" width="100%"><br>
			</div>
			<div class="col-md-offset-3 col-md-6">
		<p>3. After that you have to fill up the form or you have the option with login with social media(google,twitter) and you will have register in DOKAN Nepal</p><br>
		<img src="{{asset('public/front/img/help/5.jpg')}}" class="img-responsive" width="100%"><br>
			</div>
			<div class="col-md-offset-3 col-md-6">
			<p>4. After Registeration, you can login by clicking in the login button in the top right conner</p><br>
		<img src="{{asset('public/front/img/help/1.jpg')}}" class="img-responsive" width="100%"><br>
			</div>

			<div class="col-md-offset-3 col-md-6">
			<p>5. It will pop up the login form and you can enter email and password to login or you can use social media login to login into system</p><br>
		<img src="{{asset('public/front/img/help/3.jpg')}}" class="img-responsive" width="100%"><br>
			</div>

		<div class="col-md-12">
	<div class="help-page">
		<h3 style="font-size: 20px; margin-left: 20px;"> <b>Search Product And Order</b> </h3><br><br>
		<div class="col-md-offset-3 col-md-6">
			<p>1. click on the <b>search icon</b> and enter the product you want to buy or view</p><br>
		<img src="{{asset('public/front/img/help/6.jpg')}}" class="img-responsive" width="100%"><br>
			</div>
			<div class="col-md-offset-3 col-md-6">
			<p>2. Than click on the product</p><br>
		<img src="{{asset('public/front/img/help/8.jpg')}}" class="img-responsive" width="100%"><br>
			</div>
			<div class="col-md-offset-3 col-md-6">
			<p>3. Now you can view the product detail and click on <b>add to cart button</b> to order</p><br>
		<img src="{{asset('public/front/img/help/9.jpg')}}" class="img-responsive" width="100%"><br>
			</div>
			<div class="col-md-offset-3 col-md-6">
			<p>4. You can also add to wish list by clicking on "<b>add to wish list</b>" button</p><br>
		<img src="{{asset('public/front/img/help/7.jpg')}}" class="img-responsive" width="100%"><br>
			</div>

	</div>
</div>

</div>
</div>

@endsection