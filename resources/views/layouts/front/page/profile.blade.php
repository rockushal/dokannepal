@extends('layouts.front.app')
@section('content')
<div class="container">
	<div class="empty-space col-xs-b15 col-sm-b30"></div>
	<div class="breadcrumbs">
		<a href="#">Home</a>
		<a href="#">Profile</a>
	</div>
	<div class="empty-space col-xs-b15 col-sm-b30"></div>
	<div class="container">
		<div class="col-md-12 toppad" >


			<div class="panel panel-warning">
				<div class="panel-heading">
					<h3 class="panel-title">{{Auth::guard('customer')->user()->name}}</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{URL::to('public/images/customer/'.Auth::guard('customer')->user()->image)}}" class="img-circle img-responsive"></div>
						<div class=" col-md-9 col-lg-9 ">
							<table class="table table-user-information">
								<tbody>
									<tr>
										<td>Name</td>
										<td>{{Auth::guard('customer')->user()->name}}</td>
									</tr>
									<tr>
										<td>Date of Birth</td>
										<td>{{Auth::guard('customer')->user()->dob}}</td>
									</tr>

									<tr>
										<tr>
											<td>Gender</td>
											<td>{{Auth::guard('customer')->user()->gender}}</td>
										</tr>
										<tr>
											<td>Home Address</td>
											<td>{{Auth::guard('customer')->user()->address}}</td>
										</tr>
										<tr>
											<td>Email</td>
											<td><a href="mailto:{{Auth::guard('customer')->user()->name}}">{{Auth::guard('customer')->user()->email}}</a></td>
										</tr>
										<td>Phone Number</td>
										<td>{{Auth::guard('customer')->user()->mobile}}
										</td>

									</tr>

								</tbody>
							</table>

							<a href="{{route('password')}}" class="btn btn-warning"><i class="fa fa-key"></i> Change Password</a>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<a href="{{route('editprofile')}}" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>

				</div>

			</div>
		</div>
	</div>
</div>
</div>
@endsection
