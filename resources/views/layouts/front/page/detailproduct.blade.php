@extends('layouts.front.app')
@section('content')
<div class="container">
  <div class="empty-space col-xs-b15 col-sm-b30"></div>
  <div class="breadcrumbs">
    <a href="#">Home</a>
    <a href="#">Products</a>
    <a href="#">{{$product->name}}</a>
  </div>
  <div class="product-wrap">
    <div class="row">
      <div class="col-md-12">
        <div class="row">
          <div class="col-sm-6 col-xs-b30 col-sm-b0">
            <div class="product-img-wrap">
              <img src="{{URL::to('public/images/product/'.$product->image)}}" width="100%" alt="{{$product->name}}">
            </div>
          </div>
          <div class="col-sm-6">
            <div class="simple-article size-3 grey col-xs-b5">Product Detail</div>
            <div class="h3 col-xs-b25">{{$product->name}}</div>
            <div class="row col-xs-b25">
              <div class="col-sm-6">
                <div class="simple-article size-5 grey">Category: <span class="color">{{$product->category->name}}</span></div>
              </div>
              <div class="col-sm-6 col-sm-text-right">
                <div class="rate-wrapper align-inline">
                  @for($i=1;$i<=5;$i++)
                  @if($i<=$product->rating)
                  <i class="fa fa-star" aria-hidden="true"></i>
                  @else
                  <i class="fa fa-star-o" aria-hidden="true"></i>
                  @endif
                  @endfor
                </div>
                <div class="simple-article size-2 align-inline">Reviews Rating</div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="simple-article size-3 col-xs-b5">Product Price(NRS) <span class="grey">{{$product->price}}</span></div>
                @if(!$product->discount == null)
                <div class="simple-article size-3 col-xs-b5">Product Discount(%) <span class="grey">{{$product->discount}}%</span></div>
                <div class="simple-article size-3 col-xs-b5">Selling Price(NRS) <span class="grey"><span class="grey">{{$product->price-($product->price*$product->discount/100)}}</span></div>
                @endif
              </div>
              <div class="col-sm-6 col-sm-text-right">
                <div class="simple-article size-3 col-xs-b20">Brand: <span class="grey">{{$product->brand->name}}</span></div>
              </div>
            </div>
            @if(Auth::guard('customer')->check())
            <form method="post" action="{{route('addcart')}}">
              <div class="row col-xs-b40">
                <div class="col-sm-3">
                  <div class="h6 detail-data-title size-1">Quantity:</div>
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-sm-9">
                  <div class="quantity-select">
                    <span class="minus"></span>
                    <textarea name="item_no" style="text-align: left; padding-left: 45px;" class="number">1</textarea>
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    <input type="hidden" name="user_id" value="{{Auth::guard('customer')->user()->id}}">
                    <span class="plus"></span>
                  </div>
                </div>
              </div>
              <div class="row m5 col-xs-b40">
                <div class="col-sm-6 col-xs-b10 col-sm-b0 pull-right">
                  <button class="button size-2 style-3 block" style="border:none;" type="submit">
                    <span class="button-wrapper">
                      <span class="icon"><img src="{{URL::to('public/front/img/icon-3.png')}}" alt=""></span>
                      <span class="text">Add To Cart
                      </span>
                    </span>
                  </button>
                </div>
              </form>
              <form method="POST" action="{{route('addwishlist')}}">
                <div class="col-sm-6 col-xs-b10 col-sm-b0 pull-right">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  <input type="hidden" name="product_id" value="{{$product->id}}">
                  <input type="hidden" name="user_id" value="{{Auth::guard('customer')->user()->id}}">
                  <button class="button size-2 style-3 block" style="border:none;" type="submit">
                    <span class="button-wrapper">
                      <span class="icon"><img src="{{URL::to('public/front/img/icon-3.png')}}" alt=""></span>
                      <span class="text">Add To Wish List
                      </span>
                    </span>
                  </button>
                </div>
              </form>
            </div>

            @else
            <div class="row col-xs-b40">
              <div class="col-sm-12" style="padding-bottom: 80px;">
                <div class="h6 detail-data-title size-1">Please login with dokan nepal for ordering the product</div>
              </div>
            </div>
            @endif
          </div>
        </div>


        <div class="tabs-block">
          <div class="tabulation-menu-wrapper text-center">
            <div class="tabulation-title simple-input">Description</div>
            <ul class="tabulation-toggle">
              <li><a class="tab-menu active">Description</a></li>
              <li><a class="tab-menu">Review</a></li>
            </ul>
          </div>
          <div class="empty-space col-xs-b30 col-sm-b60"></div>

          <div class="tab-entry visible">
            <div class="product-description">
              <p>{{$product->description}}</p>
              <br>
              <h1><b>Specification</b></h1><br>
              <p>{{$product->specification}}</p>
               <div class="row"><br><center>
                 <h1><b>Share On Facebook</b></h1><br>
                 <div class="fb-share-button" data-href="http://dokannepal.com/productdetail/{{$product->slug}}" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fdokannepal.com%2Fproductdetail%2F{{$product->slug}}&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a></div></center></div>
            </div>
          </div>
          <div class="tab-entry">
            <div class="row">
              <div class="col-md-offset-3 col-md-6">
           <div class="fb-comments" data-href="http://dokannepal.com/productdetail/{{$product->slug}}" data-numposts="5"></div>
            </div>        
             </div>

          </div>
        </div>

      </div>
    </div>
  </div>

  <!-- Related Products -->

  <div class="swiper-container arrows-align-top" data-breakpoints="1" data-xs-slides="1" data-sm-slides="3" data-md-slides="4" data-lt-slides="4" data-slides-per-view="5">
    <div class="h4 swiper-title">Related Products</div>
    <div class="empty-space col-xs-b20"></div>
    <div class="swiper-button-prev style-1"></div>
    <div class="swiper-button-next style-1"></div>
    <div class="swiper-wrapper">
       @foreach($listproduct as $item)
        <div class="swiper-slide">
          <div class="product-shortcode style-1 big">
            @if(!$item->discount== null)
            <div class="product-label green">Discount {{$item->discount}}%</div>
            @endif
            <div class="preview" style="min-height: 200px; padding-top: 30px;">
              <img src="{{URL::to('public/images/product/'.$item->image)}}"  width="50%" alt="">
              <div class="preview-buttons valign-middle">
                <div class="valign-middle-content">
                  <a class="button size-2 style-3" href="{{route('listdetail',$item->slug)}}">
                    <span class="button-wrapper">
                      <span class="icon"><img src="{{URL::to('public/front/img/icon-3.png')}}" alt=""></span>
                      <span class="text">Add To Cart</span>
                    </span>
                  </a>
                </div>
              </div>
            </div>

            <div class="description">
              <div class="product-title">
                <h4>{{$item->name}}</h4>
              </div>
              <div class="simple-article text size-2">{{str_limit($item->description,20,$end="...")}}</div>
            </div>
            @if(!$item->discount== null)
            <div class="price">
              <div class="simple-article size-4"><span class="line-through">NRS {{$item->price}}</span>&nbsp;&nbsp;&nbsp;<span class="color">NRS {{$item->price-($item->price*$item->discount)/100}}</span></div>
            </div>
            @else
            <div class="price">
              <div class="simple-article size-4"><span class="color">NRS {{$item->price}}</span></div>
            </div>
            @endif
          </div>
        </div>
        @endforeach
    </div>
    <div class="swiper-pagination relative-pagination visible-xs"></div>
  </div>

  <div class="empty-space col-xs-b35 col-md-b70"></div>
  <div class="empty-space col-md-b70"></div>
</div>
@endsection
