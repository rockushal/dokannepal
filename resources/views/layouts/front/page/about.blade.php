@extends('layouts.front.app')
@section('content')
<div class="container">
  <div class="empty-space col-xs-b15 col-sm-b30"></div>
  <div class="breadcrumbs">
    <a href="#">home</a>
    <a href="#">about</a>
  </div>
</div>
<div class="empty-space col-xs-b35 col-md-b70"></div>
<div class="container">
  <div class="row">
    <div class="col-sm-5">
      <div class="simple-article size-3 grey uppercase col-xs-b5">about us</div>
      <div class="h2">Dokan Nepal</div>
      <div class="title-underline left"><span></span></div>
      <div class="simple-article size-4 grey">Praesent nec finibus massa. Phasellus id auctor lacus, at iaculis lorem. Donec quis arcu elit. In vehicula purus sem</div>
    </div>
    <div class="col-sm-7">
      <div class="simple-article size-3">
        <p>Aenean facilisis, purus ut tristique pulvinar, odio neque commodo ligula, non vestibulum lacus justo vel diam. Aenean ac aliquet tortor, nec gravida urna. Ut nec urna elit. Etiam id scelerisque ante. Cras velit nunc, luctus a volutpat nec, blandit id dolor. Quisque commodo elit nulla, eu semper quam feugiat et. Integer quam velit, suscipit eget consectetur ac, molestie eu diam.</p>
        <p>Fusce semper rhoncus dignissim. Curabitur dapibus convallis varius. Suspendisse sem urna, ullamcorper eget porttitor ut, sagittis in justo. Vestibulum egestas nulla nec purus porttitor fermentum. Integer mauris mi, viverra eget nibh at, efficitur consectetur erat. Curabitur et imperdiet enim.</p>
      </div>
    </div>
  </div>
</div>
<div class="empty-space col-xs-b35 col-md-b70"></div>


@endsection
