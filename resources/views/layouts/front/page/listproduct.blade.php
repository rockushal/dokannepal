@extends('layouts.front.app')
@section('content')

<div class="container">
  <div class="bread-crumbs-wrap">
    <div class="breadcrumbs">
      <a href="#">home</a>
      <a href="#">Shop Now</a>
      <a href="#">Products</a>
    </div>
  </div>

  <div class="row">
    <div class="col-md-9 col-md-push-3">
      <div class="products-content">
        <div class="products-wrapper">
          <div class="row nopadding">
            @foreach($productdetial as $data)
            <div class="col-sm-4">
              <div class="product-shortcode style-1">
                @if(!$data->discount== null)
                <div class="product-label green">Discount {{$data->discount}}%</div>
                @endif
                <div class="title">
                  <div class="h6 animate-to-green"><a href="#">{{$data->name}}</a></div>
                </div>
                <div class="preview">
                  <img src="{{URL::to('public/images/product/'.$data->image)}}" width="50%" alt="">
                  <div class="preview-buttons valign-middle">
                    <div class="valign-middle-content">
                      <a class="button size-2 style-3" href="{{route('listdetail',$data->slug)}}">
                        <span class="button-wrapper">
                          <span class="icon"><img src="{{URL::to('public/front/img/icon-3.png')}}" alt=""></span>
                          <span class="text">Product Detail</span>
                        </span>
                      </a>
                    </div>
                  </div>
                </div>
                @if(!$data->discount== null)
                <div class="price">
                  <div class="simple-article size-4"><span class="line-through">NRS {{$data->price}}</span>&nbsp;&nbsp;&nbsp;<span class="color">NRS {{$data->price-($data->price*$data->discount)/100}}</span></div>
                </div>
                @else
                <div class="price">
                  <div class="simple-article size-4"><span class="color">NRS {{$data->price}}</span></div>
                </div>
                @endif
                <div class="description">
                  <div class="simple-article text size-2">{{str_limit($data->description,20,$end="...")}}</div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
      <div class="empty-space col-xs-b35 col-sm-b0"></div>

      {{$productdetial->links('vendor.pagination.bootstrap-4')}}

      <div class="empty-space col-xs-b35 col-md-b70"></div>
      <div class="empty-space col-md-b70"></div>
    </div>
    <div class="col-md-3 col-md-pull-9">
      <!-- by package -->
      <div class="item__select_widget ">
        <div class="item__widget-heading">
          <h1>Product Category</h1>
        </div>
        <div class="scroll-effect">
          @foreach($categorydetail as $category)
          @if(!$category->product->count()==0)
          <label class="checkbox-entry">
            <a href="{{route('category',$category->slug)}}" class="">{{$category->name}}&nbsp;&nbsp;&nbsp; ({{$category->product->count()}})</a>
          </label>
          @endif
          @endforeach
        </div>
      </div>
      <div class="item__select_widget ">
        <div class="item__widget-heading">
          <h1>Product Brand</h1>
        </div>
        <div class="scroll-effect">
          @foreach($branddetail as $brand)
          @if(!$brand->product->count()==0)
          <label class="checkbox-entry">
            <a href="{{route('brand',$brand->slug)}}" class="">{{$brand->name}}&nbsp;&nbsp;&nbsp; ({{$brand->product->count()}})</a>

          </label>
          @endif
          @endforeach
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
