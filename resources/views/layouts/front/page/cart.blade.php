@extends('layouts.front.app')
@section('content')
<div class="cart-view-wrap">
  <div class="container">
    <!-- categories -->
      <h1 style="margin: 20px 0;font-size:  20px;font-weight: 800;">My Cart</h1>
    <div class="category-group">
      <table class="cart-table">
        <thead>
          <tr>
            <th style="width: 95px;"></th>
            <th>product name</th>
            <th style="width: 150px;">price</th>
            <th style="width: 260px;">quantity</th>
            <th style="width: 150px;">Shipping Price</th>
            <th style="width: 70px;"></th>
          </tr>
        </thead>
        <tbody>
          <?php $total=0; $item=0;?>
          @foreach($cartdetail as $key=>$cart)
          <tr>
            <td data-title=" ">
              {{$key+1}}</a>
            </td>
            <td data-title=" "><h6 class="h6"><a href="#">
              {{$cart->product->name}} </a></h6></td>
              <td data-title="Price: ">Nrs {{$cart->product->price}}</td>
              <td data-title="Quantity: ">
                {{$cart->no_of_item}}
              </td>
              <td data-title="Total:">Nrs {{$cart->product->price*$cart->no_of_item}}</td>
              <td data-title="">
                <a href="{{route('deletecart',$cart->id)}}"><div class="button-close"></div></a>
              </td>
            </tr>

            <?php
            $total=$total+$cart->product->price*$cart->no_of_item;
            $item=$item+$cart->no_of_item;
            ?>
            @endforeach
          </tbody>
        </table>
        <div class="price-block">
          <div class="subtotal-blog pull-right">
            <p class="total-price">Subtotal ({{$item}} items): <span class="pull-right">Nrs {{$total}}</span></p>
            <p class="all-total-price"><strong>Total <span class="pull-right">Nrs {{$total}}</span></strong></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endsection
