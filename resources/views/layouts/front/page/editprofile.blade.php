@extends('layouts.front.app')
@section('content')
<div class="container">
	<div class="empty-space col-xs-b15 col-sm-b30"></div>
	<div class="breadcrumbs">
		<a href="#">Home</a>
		<a href="#">Profile</a>
	</div>
	<div class="empty-space col-xs-b15 col-sm-b30"></div>
	<div class="container">
		<div class="col-md-12 toppad" >


			<div class="panel panel-warning">
				<div class="panel-heading">
					<h3 class="panel-title">{{Auth::guard('customer')->user()->name}}</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class=" col-md-12 col-lg-12 ">
							<form action="{{route('profileedit',Auth::guard('customer')->user()->id)}}" method="post" enctype="multipart/form-data">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<!-- name -->
								<div class="form-group amount">
									<div class="row">
										<div class="col-md-3">
											<div class="label-title">Name:</div>
										</div>
										<div class="col-md-9">
											<input class="simple-input" value="{{Auth::guard('customer')->user()->name}}" name="name" type="text">
										</div>
									</div>
								</div>
								<div class="form-group amount">
									<div class="row">
										<div class="col-md-3">
											<div class="label-title">Email:</div>
										</div>
										<div class="col-md-9">
											<input class="simple-input" value="{{Auth::guard('customer')->user()->email}}" type="text" readonly>
										</div>
									</div>
								</div>

								<div class="form-group amount">
									<div class="row">
										<div class="col-md-3">
											<div class="label-title">Date of Birth:</div>
										</div>
										<div class="col-md-9">
											<input class="simple-input" value="{{Auth::guard('customer')->user()->dob}}" name="dob" type="date">
										</div>
									</div>
								</div>
								<!-- contact no-->
								<div class="form-group amount">
									<div class="row">
										<div class="col-md-3">
											<div class="label-title">Gender:</div>
										</div>
										<div class="col-md-9">
											<input class="simple-input" value="{{Auth::guard('customer')->user()->gender}}" name="gender" type="text">
										</div>
									</div>
								</div>
								<div class="form-group amount">
									<div class="row">
										<div class="col-md-3">
											<div class="label-title">Home Address:</div>
										</div>
										<div class="col-md-9">
											<input class="simple-input" value="{{Auth::guard('customer')->user()->address}}" name="address" type="text">
										</div>
									</div>
								</div>
								<div class="form-group amount">
									<div class="row">
										<div class="col-md-3">
											<div class="label-title">Phone Number:</div>
										</div>
										<div class="col-md-9">
											<input class="simple-input" value="{{Auth::guard('customer')->user()->mobile}}" name="mobile" type="number">
										</div>
									</div>
								</div>
								<div class="form-group amount">
									<div class="row">
										<div class="col-md-3">
											<div class="label-title">Profile Picture:</div>
										</div>
										<div class="col-md-9">
											<input class="form-control" style="opacity: 1;" name="img" type="file">
										</div>
									</div>
								</div>
								<!-- Contact -->
								<div class="form-group use">
									<div class="row">
										<div class="col-md-3 col-lg-offset-3">
											<button class="button size-2 style-3 block" style="border: none;" href="#">
												<span class="button-wrapper">
													<span class="icon"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></span>
													<span class="text">Update</span>
												</span>
											</button>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="panel-footer">
					<a data-original-title="Broadcast Message" data-toggle="tooltip" type="button" class="btn btn-sm btn-warning"><i class="fa fa-edit"></i></a>

				</div>

			</div>
		</div>
	</div>
</div>
</div>
@endsection
