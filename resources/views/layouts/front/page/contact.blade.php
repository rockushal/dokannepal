@extends('layouts.front.app')
@section('content')
<div class="container">
  <div class="bread-crumbs-wrap">
    <div class="breadcrumbs">
      <a href="#">home</a>
      <a href="#">contact us</a>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="gift-a-coupon-wrap">
        <div class="gift-a-coupon-heading">
          <h2>Contact Us</h2>
        </div>

        <div class="gift-coupon-content-wrap">
          <form action="">
            <!-- name -->
            <div class="form-group amount">
              <div class="row">
                <div class="col-md-3">
                  <div class="label-title">Name:</div>
                </div>
                <div class="col-md-9">
                  <input class="simple-input" placeholder="Your Full Name" type="text">
                </div>
              </div>
            </div>

            <div class="form-group amount">
              <div class="row">
                <div class="col-md-3">
                  <div class="label-title">Email:</div>
                </div>
                <div class="col-md-9">
                  <input class="simple-input" placeholder="Your Email" type="email">
                </div>
              </div>
            </div>
            <!-- contact no-->
            <div class="form-group usd">
              <div class="row">
                <div class="col-md-3">
                  <div class="label-title">Contact no:</div>
                </div>
                <div class="col-md-9">
                  <input class="simple-input" placeholder="Your Contact Number" type="text">
                </div>
              </div>
            </div>
            <!-- Message -->
            <div class="form-group restaurant">
              <div class="row">
                <div class="col-md-3">
                  <div class="label-title">Message:</div>
                </div>
                <div class="col-md-9">
                  <textarea class="simple-input" placeholder="Your Message"></textarea>
                </div>
              </div>
            </div>
          </div>

          <!-- Contact -->
          <div class="form-group use">
            <div class="row">
              <div class="col-md-5 col-lg-offset-3">
                <a class="button size-2 style-2 block" href="#">
                  <span class="button-wrapper">
                    <span class="icon"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></span>
                    <span class="text">Send</span>
                  </span>
                </a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
@endsection
