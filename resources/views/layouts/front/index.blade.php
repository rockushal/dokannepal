@extends('layouts.front.app')
@section('content')

<div class="content-margins grey">
  <div class="slider-wrapper">
    <div class="swiper-button-prev visible-lg"></div>
    <div class="swiper-button-next visible-lg"></div>
    <div class="swiper-container" data-parallax="1" data-auto-height="1">
      <div class="swiper-wrapper">
        <div class="swiper-slide" style="background-image: url({{URL::to('public/front/img/background-1.png')}}">
          <div class="overlay">
            <div class="container">
              <div class="row">
                <div class="col-sm-6">
                  <div class="cell-view page-height">
                    <div class="col-xs-b40 col-sm-b80"></div>

                    <div data-swiper-parallax-x="-500">
                      <h1 class="h1 light">Reinventing the way we share</h1>
                      <div class="title-underline light left"><span></span></div>
                    </div>
                    <div data-swiper-parallax-x="-300">
                      <div class="buttons-wrapper">
                        <a class="button size-2 style-1" href="{{route('listproduct')}}">
                          <span class="button-wrapper">
                            <span class="icon"><img src="{{URL::to('public/front/img/icon-1.png')}}" alt=""></span>
                            <span class="text">Shop Now</span>
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="swiper-slide" style="background-image: url({{URL::to('public/front/img/background-2.jpg')}}">
          <div class="overlay">
            <div class="container">
              <div class="row">
                <div class="col-sm-6">
                  <div class="cell-view page-height">
                    <div class="col-xs-b40 col-sm-b80"></div>

                    <div data-swiper-parallax-x="-500">
                      <h1 class="h1 light">Reinventing the way we share</h1>
                      <div class="title-underline light left"><span></span></div>
                    </div>
                    <div data-swiper-parallax-x="-300">
                      <div class="buttons-wrapper">
                        <a class="button size-2 style-1" href="{{route('listproduct')}}">
                          <span class="button-wrapper">
                            <span class="icon"><img src="{{URL::to('public/front/img/icon-1.png')}}" alt=""></span>
                            <span class="text">Shop Now</span>
                          </span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="swiper-pagination swiper-pagination-white"></div>
    </div>
  </div>
  <div class="slider-wrapper">
    <div class="text-center gaping">
      <div class="h2">New Product</div>
    </div>
    <div class="swiper-button-prev visible-lg"></div>
    <div class="swiper-button-next visible-lg"></div>
    <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="2" data-lt-slides="3"  data-slides-per-view="4">
      <div class="swiper-wrapper">
        @foreach($product as $item)
        <div class="swiper-slide">
          <div class="product-shortcode style-1 big">
            @if(!$item->discount== null)
            <div class="product-label green">Discount {{$item->discount}}%</div>
            @endif
            <div class="preview" style="min-height: 200px; padding-top: 30px;">
              <img src="{{URL::to('public/images/product/'.$item->image)}}"  width="50%" alt="">
              <div class="preview-buttons valign-middle">
                <div class="valign-middle-content">
                  <a class="button size-2 style-3" href="{{route('listdetail',$item->slug)}}">
                    <span class="button-wrapper">
                      <span class="icon"><img src="{{URL::to('public/front/img/icon-3.png')}}" alt=""></span>
                      <span class="text">Add To Cart</span>
                    </span>
                  </a>
                </div>
              </div>
            </div>

            <div class="description">
              <div class="product-title">
                <h4>{{$item->name}}</h4>
              </div>
              <div class="simple-article text size-2">{{str_limit($item->description,20,$end="...")}}</div>
            </div>
            @if(!$item->discount== null)
            <div class="price">
              <div class="simple-article size-4"><span class="line-through">NRS {{$item->price}}</span>&nbsp;&nbsp;&nbsp;<span class="color">NRS {{$item->price-($item->price*$item->discount)/100}}</span></div>
            </div>
            @else
            <div class="price">
              <div class="simple-article size-4"><span class="color">NRS {{$item->price}}</span></div>
            </div>
            @endif
          </div>
        </div>
        @endforeach
      </div>
      <div class="swiper-pagination relative-pagination visible-xs visible-sm"></div>
    </div>
  </div>


  <div class="container">
    <div class="text-center gaping">
      <div class="h2">Product By Categories</div>
    </div>
  </div>

  <div class="tabs-block">
    <div class="container">
      <div class="tabulation-menu-wrapper text-center">
        <!-- <div class="tabulation-title simple-input">All</div> -->
        <ul class="tabulation-toggle">
          <!-- <li><a class="tab-menu active">All</a></li> -->
          <li><a class="tab-menu active">Brands</a></li>
          <li><a class="tab-menu">Popular Product</a></li>
          <li><a class="tab-menu">Offer Product</a></li>
        </ul>
      </div>
    </div>
    <div class="empty-space col-xs-b30 col-sm-b60"></div>
    <div class="tab-entry visible">
      <div class="slider-wrapper side-borders">
        <div class="swiper-button-prev hidden-xs hidden-sm"></div>
        <div class="swiper-button-next hidden-xs hidden-sm"></div>
        <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="3" data-lt-slides="4"  data-slides-per-view="5">
          <div class="swiper-wrapper">
            @foreach($brand as $brands)
            <div class="swiper-slide">
              <div class="product-shortcode style-1 big">
                <div class="preview" style="min-height: 250px; padding-top: 30px;">
                  <img src="{{URL::to('public/images/brand/'.$brands->logo)}}" width="100%" alt="">
                  <div class="preview-buttons valign-middle">
                    <div class="valign-middle-content">
                      <a class="button size-2 style-3" href="{{route('brand',$brands->slug)}}">
                        <span class="button-wrapper">
                          <span class="icon"><img src="{{URL::to('public/front/img/icon-3.png')}}" alt=""></span>
                          <span class="text">View Product</span>
                        </span>
                      </a>
                    </div>
                  </div>
                </div>

                <div class="description">
                  <div class="product-title">
                    <h4>{{$brands->name}}</h4>
                  </div>
                  <div class="simple-article text size-2">{{str_limit($brands->description,20,$end="...")}}</div>
                </div>
              </div>
            </div>
            @endforeach

          </div>
          <div class="swiper-pagination relative-pagination visible-xs visible-sm"></div>
        </div>
      </div>
    </div>
    <div class="tab-entry">
      <div class="slider-wrapper side-borders">
        <div class="swiper-button-prev hidden-xs hidden-sm"></div>
        <div class="swiper-button-next hidden-xs hidden-sm"></div>
        <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="3" data-lt-slides="4"  data-slides-per-view="5">
          <div class="swiper-wrapper">
            @foreach($productrate as $data)
            <div class="swiper-slide">
              <div class="product-shortcode style-1 big">
                @if(!$data->discount== null)
                <div class="product-label green">Discount {{$data->discount}}%</div>
                @endif
                <div class="preview" style="min-height: 250px; padding-top: 30px;">
                  <img src="{{URL::to('public/images/product/'.$data->image)}}"  width="100%" alt="">
                  <div class="preview-buttons valign-middle">
                    <div class="valign-middle-content">
                      <a class="button size-2 style-3" href="{{route('listdetail',$data->slug)}}">
                        <span class="button-wrapper">
                          <span class="icon"><img src="{{URL::to('public/front/img/icon-3.png')}}" alt=""></span>
                          <span class="text">Add To Cart</span>
                        </span>
                      </a>
                    </div>
                  </div>
                </div>

                <div class="description">
                  <div class="product-title">
                    <h4>{{$data->name}}</h4>
                  </div>
                  <div class="simple-article">{{str_limit($data->description,20,$end="...")}}</div>
                </div>
                @if(!$data->discount== null)
                <div class="price">
                  <div class="simple-article size-4"><span class="line-through">NRS {{$data->price}}</span>&nbsp;&nbsp;&nbsp;<span class="color">NRS {{$data->price-($data->price*$data->discount)/100}}</span></div>
                </div>
                @else
                <div class="price">
                  <div class="simple-article size-4"><span class="color">NRS {{$data->price}}</span></div>
                </div>
                @endif
              </div>
            </div>
            @endforeach

          </div>
          <div class="swiper-pagination relative-pagination visible-xs visible-sm"></div>
        </div>
      </div>
    </div>
    <div class="tab-entry">
      <div class="slider-wrapper side-borders">
        <div class="swiper-button-prev hidden-xs hidden-sm"></div>
        <div class="swiper-button-next hidden-xs hidden-sm"></div>
        <div class="swiper-container" data-breakpoints="1" data-xs-slides="1" data-sm-slides="2" data-md-slides="3" data-lt-slides="4"  data-slides-per-view="5">
          <div class="swiper-wrapper">
            @foreach($productdiscount as $data)
            <div class="swiper-slide">
              <div class="product-shortcode style-1 big">
                @if(!$data->discount== null)
                <div class="product-label green">Discount {{$data->discount}}%</div>
                @endif
                <div class="preview" style="min-height: 250px; padding-top: 30px;">
                  <img src="{{URL::to('public/images/product/'.$data->image)}}"  width="100%" alt="">
                  <div class="preview-buttons valign-middle">
                    <div class="valign-middle-content">
                      <a class="button size-2 style-3" href="{{route('listdetail',$data->slug)}}">
                        <span class="button-wrapper">
                          <span class="icon"><img src="{{URL::to('public/front/img/icon-3.png')}}" alt=""></span>
                          <span class="text">Add To Cart</span>
                        </span>
                      </a>
                    </div>
                  </div>
                </div>

                <div class="description">
                  <div class="product-title">
                    <h4>{{$data->name}}</h4>
                  </div>
                  <div class="simple-article">{{str_limit($data->description,20,$end="...")}}</div>
                </div>
                @if(!$data->discount== null)
                <div class="price">
                  <div class="simple-article size-4"><span class="line-through">NRS {{$data->price}}</span>&nbsp;&nbsp;&nbsp;<span class="color">NRS {{$data->price-($data->price*$data->discount)/100}}</span></div>
                </div>
                @else
                <div class="price">
                  <div class="simple-article size-4"><span class="color">NRS {{$data->price}}</span></div>
                </div>
                @endif
              </div>
            </div>
            @endforeach
          </div>
          <div class="swiper-pagination relative-pagination visible-xs visible-sm"></div>
        </div>
      </div>
    </div>
  </div>
  <div class="empty-space col-xs-b40"></div>
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-sm-offset-6">
        <div class="cell-view simple-banner-height text-center">
          <div class="empty-space col-sm-b35"></div>
          <div class="simple-article grey uppercase size-5 col-xs-b5"><span class="color">special offers</span> for subscribers</div>
          <h3 class="h3 col-xs-b15">New Offers Every Week <span class="color">+</span> Discount System <span class="color">+</span> Best Hot Prices</h3>
          <div class="simple-article size-3 col-xs-b25 col-sm-b50">Praesent nec finibus massa. Phasellus id auctor lacus, at iaculis lorem. Donec quis arcu elit. In vehicula purus sem, eu mattis est lacinia sit amet.</div>
          <div class="single-line-form">
            <input class="simple-input" type="text" value="" placeholder="Enter your email">
            <div class="button size-2 style-3">
              <span class="button-wrapper">
                <span class="icon"><img src="{{URL::to('public/front/img/icon-4.png')}}" alt=""></span>
                <span class="text">submit</span>
              </span>
              <input type="submit" value="">
            </div>
          </div>
          <div class="empty-space col-xs-b35"></div>
        </div>
      </div>
    </div>
    <div class="row-background left hidden-xs">
      <img src="{{URL::to('public/front/img/products/newsletter.jpg')}}" alt="" />
    </div>
  </div>
</div>
@endsection
