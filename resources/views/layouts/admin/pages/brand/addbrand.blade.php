@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / <a href="{{route('brand.index')}}"><span class="nav-link-text">Brands</span></a> / Add Brand</li>
      </ol>      
          <div class="row">
            <div class="col-lg-12">
              <form role="form" method="post" action="{{route('brand.store')}}" enctype="multipart/form-data">
                <div class="row p-3">
                <div class="col-lg-12">
                <div class="form-group">
                <label>Brand Name</label>
                <input class="form-control" name="brandname" placeholder="Enter Brand Name">
                </div>
              </div>
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="col-lg-12">
                <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" placeholder="Enter Brand Description" rows="3"></textarea>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Brand Logo (PNG,JPG) : </label>
                <input type="file" name="img">
                </div>
              </div>
              
              <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('brand.index') }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>              
              </form>
              </div>
          </div>
    </div>
  </div>
    
@endsection
