@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / Order</li>
      </ol>
       @if(Session::has('success'))
          <div class="alert alert-success text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('success')}}
        </div>
          @endif
          @if(Session::has('error'))
          <div class="alert alert-danger text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('error')}}
        </div>
          @endif
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Order</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Customer Name</th>
                  <th>Address</th>
                  <th>Phone No</th>
                  <th>Product Name</th>
                  <th>Product Image</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>S.No</th>
                  <th>Customer Name</th>
                  <th>Address</th>
                  <th>Phone No</th>
                  <th>Product Name</th>
                  <th>Product Image</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($order as $key=>$data)
                <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$data->user->name}}</td>
                  <td>{{$data->user->address}}</td>
                  <td>{{$data->user->mobile}}</td>
                  <td>{{$data->product->name}}</td>
                  <td><img src="{{URL::to('public/images/product/'.$data->product->image)}}" width="50px" class="img-responsive"></td>
                   </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
          
    </div>
    
@endsection
