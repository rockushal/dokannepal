@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / Advertisement</li>
      </ol>
      <div class="row">
        <div class="col-md-12">
          <a class="btn btn-primary" href="{{ route('advertisement.create') }}" style="float: right; margin: 10px; color: #fff;">ADD</a>
        </div>
      </div>
       @if(Session::has('success'))
          <div class="alert alert-success text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('success')}}
        </div>
          @endif
          @if(Session::has('error'))
          <div class="alert alert-danger text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('error')}}
        </div>
          @endif
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Example</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>S.No.</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>S.No.</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach ($advertisement as $key=>$data)
                <tr>
                   <td>{{$key+1}}</td>
                    <td>{{$data->name}}</td>
                    <td>{{$data->description}}</td>
                    
                    <td><img src="{{URL::to('public/images/advertisement/'.$data->image)}}" width="50px" class="img-responsive"></td>
                    <td class="text-center">{!! Form::open(['class'=>'delete_form','method'=>'DELETE','action' =>['advertisement\advertisementController@destroy',$data->id]]) !!}
                    <div class="form-group">
                            {{Form::button('<i class="fa fa-trash"></i>',['class'=>'delete_btn','type'=>'submit','onclick'=>'return confirm("Do you want to delete")'])}}
                    </div>
                     {!! Form::close() !!}
                        <a href="{{route('advertisement.edit', $data->id) }}"><i class="fa fa-pencil-square-o"></i></a>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
          
    </div>
    
@endsection
