@extends('layouts.app')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / <a href="{{route('advertisement.index')}}"><span class="nav-link-text">Advertisments</span></a> / Add Advertisment</li>
      </ol>      
          <div class="row">
            <div class="col-lg-12">
              <form role="form" method="post" action="{{route('advertisement.store')}}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row p-3">
                <div class="col-lg-6">
                <div class="form-group">
                <label>Advertisment Name</label>

                <input class="form-control" name="name" placeholder="Enter Advertisment Name">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" placeholder="Enter Advertisment Description" rows="3"></textarea>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Product Image (PNG,JPG) : </label>
                <input type="file" name="img">
                </div>
              </div>
              <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('advertisement.index') }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>              
              </form>
              </div>
          </div>
    </div>
  </div>
    
@endsection
