@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / <a href="{{route('advertisement.index')}}"><span class="nav-link-text">Advertisements</span></a> / Edit Advertisement</li>
      </ol>      
          <div class="row">
            <div class="col-lg-12">
              {!! Form::model($editadvertisement,array('route'=>['advertisement.update',$editadvertisement->id],'method'=>'PUT', 'files'=>'true'))!!}
                <div class="row p-3">
                <div class="col-lg-6">
                <div class="form-group">
                <label>advertisement Name</label>
                <input class="form-control" name="name" value="{{$editadvertisement->name}}">
                </div>
              </div>
              <div class="col-lg-12">
                <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control" rows="3">{{$editadvertisement->description}}</textarea>
                </div>
              </div>              
              <div class="col-lg-6">
                <div class="form-group">
                <label>advertisement Image (PNG,JPG) : </label>
                <input type="file" name="img">
                </div>
              </div>
              <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('advertisement.index') }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>              
              {!! Form::close() !!}
              </div>
          </div>
    </div>
  </div>
    
@endsection
