@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / <a href="{{route('category.index')}}"><span class="nav-link-text">Categorys</span></a> / Add Category</li>
      </ol>      
          <div class="row">
            <div class="col-lg-12">
              <form role="form" method="post" action="{{route('category.store')}}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row p-3">
                <div class="col-lg-6">
                <div class="form-group">
                <label>Category Name</label>
                <input class="form-control" name="name" placeholder="Enter Category Name">
                </div>
              </div>
              <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('category.index') }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>              
              </form>
              </div>
          </div>
    </div>
  </div>
    
@endsection
