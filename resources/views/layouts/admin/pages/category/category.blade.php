@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / Categorys</li>
      </ol>
      <div class="row">
        <div class="col-md-12">
          <a class="btn btn-primary" href="{{ route('category.create') }}" style="float: right; margin: 10px; color: #fff;">ADD</a>
        </div>
      </div>
       @if(Session::has('success'))
          <div class="alert alert-success text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('success')}}
        </div>
          @endif
          @if(Session::has('error'))
          <div class="alert alert-danger text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('error')}}
        </div>
          @endif
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Data Table Category</div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>S.No</th>
                  <th>Category Name</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>S.No</th>
                  <th>Category Name</th>
                  <th>Action</th>
                </tr>
              </tfoot>
              <tbody>
                @foreach($category as $key=>$data)
                <tr>
                  <td>{{$key+1}}</td>
                  <td>{{$data->name}}</td>
                  <td>{!! Form::open(['class'=>'delete_form','method'=>'DELETE','action' =>['category\categoryController@destroy',$data->id]]) !!}
                      <div class="form-group">
                              {{Form::button('<i class="fa fa-trash"></i>',['class'=>'delete_btn','type'=>'submit','onclick'=>'return confirm("Do you want to delete")'])}}
                      </div>
                       {!! Form::close() !!}
                          <a href="{{route('category.edit', $data->id) }}"><i class="fa fa-pencil-square-o"></i></a></td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
      </div>
          
    </div>
    
@endsection
