@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / Settings
        </li>
      </ol> 
      @if(Session::has('success'))
          <div class="alert alert-success text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('success')}}
        </div>
          @endif
          @if(Session::has('error'))
          <div class="alert alert-danger text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('error')}}
        </div>
          @endif     
          <div class="row">
            <div class="col-lg-12">
              {!! Form::model($setting,array('route'=>['setting.update',$setting->id],'method'=>'PUT', 'files'=>'true'))!!}
                <div class="row p-3">
                <div class="col-lg-6">
                <div class="form-group">
                <label>Title</label>
                <input type="hidden" name="user_id" value="{{Auth::id()}}">
                <input class="form-control" name="title" value="{{$setting->title}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Email</label>
              <input type="Email" class="form-control" name="email" value="{{$setting->email}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Phone</label>
                <input class="form-control" name="phone" value="{{$setting->phone}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Mobile</label>
                <input class="form-control" name="mobile" value="{{$setting->mobile}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Address</label>
                <input class="form-control" name="address" value="{{$setting->Address}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Facebook Link</label>
                <input class="form-control" name="facebook" value="{{$setting->facebook_link}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Twitter link</label>
                <input class="form-control" name="twitter" value="{{$setting->twitter}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Google Link</label>
                <input class="form-control" name="google" value="{{$setting->goole_plus}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Logo (PNG,JPG) : </label>
                <input type="file" name="logo">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>favicon (PNG,JPG) : </label>
                <input type="file" name="favicon">
                </div>
              </div>
              <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('home') }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>              
              {!! Form::close() !!}
              </div>
          </div>
    </div>
  </div>
    
@endsection
