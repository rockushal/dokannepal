@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / Change Password</li>
      </ol>      
       @if(Session::has('success'))
          <div class="alert alert-success text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('success')}}
        </div>
          @endif
          @if(Session::has('error'))
          <div class="alert alert-danger text-center" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          {{Session('error')}}
        </div>
          @endif
          <div class="row">
            <div class="col-lg-12">
               {!! Form::model($admin,array('route'=>['changepassword.update',$admin->id],'method'=>'PUT', 'files'=>'true'))!!}
                <div class="row p-3">
                  <div class="col-lg-12">
                <div class="col-lg-6">
                <div class="form-group">
                <label>Email</label>
                <input type="email" value="{{$admin->email}}" class="form-control" readonly>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Current Password</label>
                <input type="password" name="old_password" class="form-control" placeholder="Current Password">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>New Password</label>
                <input type="password" name="new_password" class="form-control" placeholder="New Password">
                </div>
              </div>
              </div>
              
              <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('home') }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>              
              {!! Form::close() !!}
              </div>
          </div>
    </div>
  </div>
    
@endsection
