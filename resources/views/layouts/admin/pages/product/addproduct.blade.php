@extends('layouts.app')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / <a href="{{route('product.index')}}"><span class="nav-link-text">Products</span></a> / Add Product</li>
      </ol>      
          <div class="row">
            <div class="col-lg-12">
              <form role="form" method="post" action="{{route('product.store')}}" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row p-3">
                <div class="col-lg-6">
                <div class="form-group">
                <label>Product Name</label>

                <input class="form-control" name="name" placeholder="Enter Product Name">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Rating</label>
                <select class="form-control" name="rating">
                    <option value="0">--Rate A Product--</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                </select>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Description</label>
                <textarea class="form-control" name="description" placeholder="Enter Product Description" rows="3"></textarea>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Specification</label>
                <textarea class="form-control" name="specification" placeholder="Enter Product Description" rows="3"></textarea>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Category</label>
                <select class="form-control" name="category">
                    <option value="">--Choose a Category--</option>
                    @foreach($category as $category_data)
                    <option value="{{$category_data->id}}">{{$category_data->name}}</option>
                    @endforeach
                </select>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Brand</label>
                <select class="form-control" name="brand">
                    <option value="">--Choose a Brand--</option>
                    @foreach($brand as $brand_data)
                    <option value="{{$brand_data->id}}">{{$brand_data->name}}</option>
                    @endforeach
                </select>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Price(Nrs)</label>
                <input class="form-control" type="number" name="price" placeholder="Enter Product Price">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Discount(%)</label>
                <input class="form-control" type="number" name="discount" placeholder="Enter Discount Percentage e.g.(10)">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Product Image (PNG,JPG) : </label>
                <input type="file" name="img">
                </div>
              </div>
              <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Submit</button>
                <a href="{{ route('product.index') }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>              
              </form>
              </div>
          </div>
    </div>
  </div>
    
@endsection
