@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
      <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item active"><a href="{{route('home')}}"><span class="nav-link-text">Dashboard</span></a> / <a href="{{route('product.index')}}"><span class="nav-link-text">Products</span></a> / Edit Product</li>
      </ol>      
          <div class="row">
            <div class="col-lg-12">
              {!! Form::model($editproduct,array('route'=>['product.update',$editproduct->id],'method'=>'PUT', 'files'=>'true'))!!}
                <div class="row p-3">
                <div class="col-lg-6">
                <div class="form-group">
                <label>Product Name</label>
                <input class="form-control" name="name" value="{{$editproduct->name}}">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Rating</label>
                <select class="form-control" name="rating">
                    <option value="0">--Choose a rating--</option>
                    @if(!$editproduct->rating=="")
                    <option value="{{$editproduct->rating}}" selected>{{$editproduct->rating}}</option>
                    @endif
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>

                </select>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Description</label>
                <textarea name="description" class="form-control" rows="3">{{$editproduct->description}}</textarea>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Specification</label>
                <textarea name="specification" class="form-control" rows="3">{{$editproduct->specification}}</textarea>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Category</label>
                <select name="Category" class="form-control">
                    <option value="">--Choose a Category--</option>

                    @foreach ($category as $categorie)
                    @if($editproduct->category_id=="")
                    <option value="{{$categorie->id}}">{{$categorie->name}}</option>
                    @else
                    <option {{old('category',$editproduct->category->id)=="$categorie->id"? 'selected':''}} value="{{$categorie->id}}">{{$categorie->name}}</option>
                    @endif
                @endforeach
                </select>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Brand</label>
                <select name="brand" class="form-control">
                    <option value="">--Choose a Brand--</option>
                    @foreach ($brand as $brands)
                    @if($editproduct->brand_id=="")
                    <option value="{{$brands->id}}">{{$brands->name}}</option>
                    @else
                    <option {{old('brand',$editproduct->brand->id)=="$brands->id"? 'selected':''}} value="{{$brands->id}}">{{$brands->name}}</option>
                    @endif
                @endforeach
                </select>
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Price</label>
                <input name="price" type="number" value="{{$editproduct->price}}" class="form-control">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Discount</label>
                <input name="discount" type="number" value="{{$editproduct->discount}}" class="form-control">
                </div>
              </div>
              <div class="col-lg-6">
                <div class="form-group">
                <label>Product Image (PNG,JPG) : </label>
                <input type="file" name="img">
                </div>
              </div>
              <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('product.index') }}" class="btn btn-danger">Cancel</a>
                </div>
              </div>              
              {!! Form::close() !!}
              </div>
          </div>
    </div>
  </div>
    
@endsection
