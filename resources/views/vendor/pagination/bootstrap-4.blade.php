@if ($paginator->hasPages())
<div class="row">
  {{-- Previous Page Link --}}
  @if ($paginator->onFirstPage())
  <div class="col-sm-3 hidden-xs">
    <a class="button size-1 style-5 disabled">
      <span class="button-wrapper">
        <span class="icon"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
        <span class="text">prev page</span>
      </span>
    </a>
  </div>
  <!-- <li class="page-item disabled"><span class="page-link">&laquo;</span></li> -->
  @else
  <!-- <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" >&laquo;</a></li> -->
  <div class="col-sm-3 hidden-xs">
    <a class="button size-1 style-5" href="{{ $paginator->previousPageUrl() }}" rel="prev">
      <span class="button-wrapper">
        <span class="icon"><i class="fa fa-angle-left" aria-hidden="true"></i></span>
        <span class="text">prev page</span>
      </span>
    </a>
  </div>
  @endif

  {{-- Pagination Elements --}}
  @foreach ($elements as $element)
  {{-- "Three Dots" Separator --}}
  @if (is_string($element))
  <div class="col-sm-6 text-center">
    <div class="pagination-wrapper">

      <span class="pagination">...</span>
    </div>
  </div>
  @endif

  {{-- Array Of Links --}}
  @if (is_array($element))
  <div class="col-sm-6 text-center">
    <div class="pagination-wrapper">
      @foreach ($element as $page => $url)
      @if ($page == $paginator->currentPage())

      <a class="pagination active">{{ $page }}</a>


      <!-- <li class="page-item active"><span class="page-link">{{ $page }}</span></li> -->
      @else
      <a class="pagination" href="{{ $url }}">{{ $page }}</a>
      <!-- <li class="page-item"><a class="page-link" >{{ $page }}</a></li> -->
      @endif
      @endforeach
    </div>
  </div>
  @endif
  @endforeach

  {{-- Next Page Link --}}
  @if ($paginator->hasMorePages())
  <div class="col-sm-3 hidden-xs text-right">
    <a class="button size-1 style-5" href="{{ $paginator->nextPageUrl() }}">
      <span class="button-wrapper">
        <span class="icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
        <span class="text">Next page</span>
      </span>
    </a>
  </div>
  <!-- <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li> -->
  @else
  <div class="col-sm-3 hidden-xs text-right">
    <a class="button size-1 style-5">
      <span class="button-wrapper">
        <span class="icon"><i class="fa fa-angle-right" aria-hidden="true"></i></span>
        <span class="text">Next page</span>
      </span>
    </a>
  </div>
  @endif
</div>
@endif
