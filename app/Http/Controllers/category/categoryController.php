<?php

namespace App\Http\Controllers\category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Repository\Category\CategoryRepositoryInterface;

class categoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(CategoryRepositoryInterface $Category)
    {
        $this->middleware('auth');
        $this->Category=$Category;

    }
    public function index()
    {
        $category=$this->Category->index();
        return view('layouts/admin/pages/category/category', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts/admin/pages/category/addcategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $Categorydetails['name']=$request->name;
            $Categorydetails['slug']=str_slug($Categorydetails['name'], '-');
            $this->Category->create($Categorydetails);
            return redirect()->route('category.index')->with('success', 'Your Category has been successfully Created.');
        }
        catch(Exception $e)
        {
            dd($e);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->Category->find($id);
        return view('layouts/admin/pages/category/editcategory' , compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try{
            $Categorydetails['name']=$request->name;
            $Categorydetails['slug']=str_slug($Categorydetails['name'], '-');
            $this->Category->update($id,$Categorydetails);
            return redirect()->route('category.index')->with('success', 'Your Category has been successfully Update.');
        }
        catch(Exception $e)
        {
            dd($e);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->Category->delete($id);
        return redirect()->route('category.index')->with('success', 'Your category has been successfully deleted.');
    
    }
}
