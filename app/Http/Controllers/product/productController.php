<?php namespace App\Http\Controllers\product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Repository\Product\ProductRepositoryInterface;
use App\Repo\Repository\Category\CategoryRepositoryInterface;
use App\Repo\Repository\Brand\BrandRepositoryInterface;
use App\Repo\Repository\Cart\CartRepositoryInterface;


class productController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function __construct(ProductRepositoryInterface $product,CategoryRepositoryInterface $category,BrandRepositoryInterface $brand,CartRepositoryInterface $cart)
  {
    $this->middleware('auth');
    $this->category=$category;
    $this->brand= $brand;
    $this->product=$product;
    $this->cart=$cart;
  }
  public function index()
  {
    $product = $this->product->index();
    return view('layouts/admin/pages/product/product', compact('product'));
  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    $brand= $this->brand->index();
    $category=$this->category->index();
    return view('layouts/admin/pages/product/addproduct',compact('brand','category'));
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    try{
      if($request->hasFile('img'))
      {

        $imgName = time().'-'.$request->file('img')->getClientOriginalName();
        $path = 'public/images/product/';
        $request->file('img')->move($path,$imgName);
        $productdetails['image']=$imgName;
      }
      $productdetails['name']=$request->name;
      $productdetails['description']=$request->description;
      $productdetails['price']=$request->price;
      $productdetails['specification']=$request->specification;
      $productdetails['category_id']=$request->category;
      $productdetails['brand_id']=$request->brand;
      $productdetails['slug']=str_slug($productdetails['name'].'-'.str_random(4), '-');
      $productdetails['rating']=$request->rating;
      $productdetails['discount']=$request->discount;
      $product=$this->product->create($productdetails);
      return redirect()->route('product.index')->with('success', 'Your Product has been successfully Added.');
    }
    catch(\Exception $e){
      dd($e);
    }
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    $brand= $this->brand->index();
    $category=$this->category->index();
    $editproduct=$this->product->find($id);
    return view('layouts.admin.pages.product.editproduct',compact('editproduct','brand','category'));
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try{

      if($request->hasFile('img'))
      {
        //deleting image
        $product=$this->product->find($id);
        if(!$product->image == null){
          unlink(public_path()."/images/product/".$product->image);
        }
        $imgName = time().'-'.$request->file('img')->getClientOriginalName();
        $path = 'public/images/product/';
        $request->file('img')->move($path,$imgName);
        $productdetails['image']=$imgName;
      }
      $productdetails['name']=$request->name;
      $productdetails['description']=$request->description;
      $productdetails['price']=$request->price;
      $productdetails['rating']=$request->rating;
      $productdetails['discount']=$request->discount;
      $productdetails['specification']=$request->specification;
      $productdetails['category_id']=$request->Category;
      $productdetails['brand_id']=$request->brand;
      $productdetails['slug']=str_slug($productdetails['name'].'-'.str_random(4), '-');
      // dd($productdetails);
      $user=$this->product->update($id,$productdetails);
      // dd($user);
      return redirect()->route('product.index')->with('success', 'Your Product has been successfully Updated.');
    }
    catch(Exception $e){
      dd($e);
    }
  }

  public function order(){
    $order=$this->cart->index();
    return view('layouts.admin.pages.order.order',compact('order'));
  }
  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    $product=$this->product->find($id);
    // dd($event);
    if(!$product->image == ""){
      unlink(public_path()."/images/product/".$product->image);
    }
    $this->product->delete($id);
    return redirect()->route('product.index')->with('error', 'Your product has been successfully deleted.');
  }
}
