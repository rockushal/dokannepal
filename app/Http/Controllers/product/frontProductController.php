<?php

namespace App\Http\Controllers\product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Repository\Product\ProductRepositoryInterface;
use App\Repo\Repository\Category\CategoryRepositoryInterface;
use App\Repo\Repository\Brand\BrandRepositoryInterface;
use App\Repo\Repository\Cart\CartRepositoryInterface;
use App\Repo\Repository\Wish\WishRepositoryInterface;
use Auth;

class frontProductController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function __construct(ProductRepositoryInterface $product,CategoryRepositoryInterface $category,BrandRepositoryInterface $brand,CartRepositoryInterface $cart,WishRepositoryInterface $wish)
  {
    $this->product=$product;
    $this->category=$category;
    $this->brand=$brand;
    $this->cart=$cart;
    $this->wish=$wish;

  }
  public function showListProduct()
  {
    $productdetial = $this->product->listproduct();
    $categorydetail = $this->category->index();
    $branddetail = $this->brand->index();
    return view('layouts/front/page/listproduct', compact('productdetial','categorydetail','branddetail'));
  }

  public function showListProductByBrand($slug)
  {
    $id= $this->brand->getIdBySlug($slug);
    $productdetial=$this->product->getbrandproduct($id);
    // dd($productdetial);
    $categorydetail = $this->category->index();
    $branddetail = $this->brand->index();
    return view('layouts/front/page/listproduct', compact('productdetial','categorydetail','branddetail'));
  }

  public function showListProductByCategory($slug)
  {
    $id= $this->category->getIdBySlug($slug);
    $productdetial=$this->product->getcategoryproduct($id);
    // dd($productdetial);
    $categorydetail = $this->category->index();
    $branddetail = $this->brand->index();
    return view('layouts/front/page/listproduct', compact('productdetial','categorydetail','branddetail'));
  }
  public function showSearchProduct(Request $request)
  {
    $productdetial=$this->product->searchProduct($request->value);
    $categorydetail = $this->category->index();
    $branddetail = $this->brand->index();
    return view('layouts/front/page/listproduct', compact('productdetial','categorydetail','branddetail'));
  }
  public function showProductDetail($slug)
  {
    $product=$this->product->findBySlug($slug);
    $listproduct=$this->product->getcategoryproduct($product->category_id);
    return view('layouts/front/page/detailproduct', compact('product','listproduct'));
  }
  public function showContact(){
    return view('layouts/front/page/contact');
  }

  public function showAbout(){
    return view('layouts/front/page/about');
  }
  /* After User Login */
  public function addToCart(Request $request){
    try{
      if (Auth::guard('customer')->check()) {
        $cartdetails['user_id']=$request->user_id;
        $cartdetails['product_id']=$request->product_id;
        $cartdetails['no_of_item']=$request->item_no;
        $cart=$this->cart->create($cartdetails);
        return redirect()->back()->with('success', 'Item has been successfully Added.');
      }
      else{
        return redirect()->route('main')->with('error', 'User has not been Login');
      }
    }
    catch(\Exception $e){
      dd($e);
    }
  }
  public function cartView($id){
    if (Auth::guard('customer')->check()) {
      $cartdetail = $this->cart->getCartByUserId($id);
      return view('layouts/front/page/cart', compact('cartdetail'));
    }
    else{
      return redirect()->route('main')->with('error', 'User has not been Login');
    }
  }

  public function cartDelete($id){
    if (Auth::guard('customer')->check()) {
      $this->cart->delete($id);
      return redirect()->back()->with('error', 'Cart has been successfully deleted.');
    }
    else{
      return redirect()->route('main')->with('error', 'User has not been Login');
    }
  }

  public function wishView($id){
    if (Auth::guard('customer')->check()) {
      $wishdetail = $this->wish->getWishByUserId($id);
      return view('layouts/front/page/wishlist', compact('wishdetail'));
    }
    else{
      return redirect()->route('main')->with('error', 'User has not been Login');
    }
  }

  public function wishDelete($id){
    if (Auth::guard('customer')->check()) {
      $this->wish->delete($id);
      return redirect()->back()->with('error', 'wish has been successfully deleted.');
    }
    else{
      return redirect()->route('main')->with('error', 'User has not been Login');
    }
  }

  public function addToWish(Request $request){
    try{
      if (Auth::guard('customer')->check()) {
        $wish=$this->wish->getwish($request->user_id,$request->product_id);
        if($wish == null){
          $wishdetails['user_id']=$request->user_id;
          $wishdetails['product_id']=$request->product_id;
          $wish=$this->wish->create($wishdetails);
          return redirect()->back()->with('success', 'Item has been successfully Added.');
        }
        else{
          return redirect()->back()->with('error', 'Item has already been Added.');
        }
      }
      else{
        return redirect()->route('main')->with('error', 'User has not been Login');
      }
    }
    catch(\Exception $e){
      dd($e);
    }
  }
  public function showHelp(){
    return view('layouts.front.page.help');
  }
  public function home()
  {
    return view('welcome');
  }
}
