<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Repository\Admin\AdminRepositoryInterface;
use Auth;
use Illuminate\Support\Facades\Hash;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct(AdminRepositoryInterface $admin)
    {
        $this->middleware('auth');
        $this->admin=$admin;

    }
    public function index()
    {
        $admin=$this->admin->find(Auth::id());
        return view('layouts/admin/pages/password/changepassword', compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(strlen($request->new_password)>= 6){
            $admin=$this->admin->find($id);
           if (Hash::check($request->old_password, $admin->password)){
            $admindetails['password']=bcrypt($request->new_password);
            $this->admin->update($id,$admindetails);
            return redirect()->back()->with('success', 'Change Password ');
           }
           else {
              return redirect()->back()->with('error', 'Incorrect Password ');
           }
       }
       else{
        return redirect()->back()->with('error', 'Please enter the atleast 6 character Password ');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
