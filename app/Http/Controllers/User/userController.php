<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Repository\Customer\CustomerRepositoryInterface;

class userController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function __construct(CustomerRepositoryInterface $customer)
  {
    $this->customer = $customer;
  }
  public function index()
  {

  }

  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {

  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    try{

      if($request->hasFile('img')){
        //deleting image
        $customer=$this->customer->find($id);
        if(!$customer->image == null){
          unlink(public_path()."/images/customer/".$customer->image);
        }


        $imgName = time().'-'.$request->file('img')->getClientOriginalName();
        $path = 'public/images/customer/';
        $request->file('img')->move($path,$imgName);
        $customerdetails['image']=$imgName;
      }
      $customerdetails['dob']=$request->dob;
      $customerdetails['gender']=$request->gender;
      $customerdetails['address']=$request->address;
      $customerdetails['mobile']=$request->mobile;
      $user=$this->customer->update($id,$customerdetails);
      // dd($user);
      return redirect()->route('profile')->with('success', 'Your Product has been successfully Updated.');
    }
    catch(Exception $e){
      dd($e);
    }
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
  public function showProfile(){
    return view('layouts/front/page/profile');
  }
  public function showProfileEdit(){
    return view('layouts/front/page/editprofile');
  }
  public function showpassword(){
    return view('layouts/front/page/changepassword');
  }
  public function changepassword(Request $request, $id)
  {
    if(strlen($request->password)>= 6){
      if ($request->password==$request->confirm){
        $customerdetails['password']=bcrypt($request->password);
        $this->customer->update($id,$customerdetails);
        return redirect()->back()->with('success', 'Password update success');
      }
      else {
        return redirect()->back()->with('error', 'Password Mismatch');
      }
    }
    else{
      return redirect()->back()->with('error', 'Please enter the atleast 6 character Password ');
    }
  }
  public function userRegister(Request $request){
    $authUser = $this->customer->finduser($request->email);
    if ($authUser==null) {
      if($request->password==$request->confirm){
        $this->customer->create([
          'name'     => $request->name,
          'email'    => $request->email,
          'password' => bcrypt($request->password)
        ]);
        return redirect()->back()->with('success', 'User has been register');
      }
      else{
        return redirect()->back()->with('error', 'Password Mismatch');
      }
    }
    else{
      return redirect()->back()->with('error', 'Email already exist');
    }
  }
}
