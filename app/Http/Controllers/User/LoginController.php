<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
use App\Repo\Repository\Customer\CustomerRepositoryInterface;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct(CustomerRepositoryInterface $customer)
    {
        $this->customer = $customer;
        $this->middleware('guest:customer')->except('logout');
    }
    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('customer');
    }

     public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        if($provider == "twitter"){
        $user = Socialite::driver($provider)->user();
    }
    else{
        $user = Socialite::driver($provider)->stateless()->user();
    }
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::guard('customer')->login($authUser, true);
        return redirect($this->redirectTo);
    }

    public function findOrCreateUser($user, $provider)
    {
        if($provider == "twitter"){
        $authUser = $this->customer->finduser($user->nickname."@gmail.com");
        if (!$authUser==null) {
            return $authUser;
        }
        else{
        $this->customer->create([
            'name'     => $user->name,
            'email'    => $user->nickname."@gmail.com",
            'provider' => $provider,
            'provider_id' => $user->id,
            'password' => ''
        ]);
        $authUser = $this->customer->finduser($user->nickname."@gmail.com");
        return $authUser;

    }
}
else{
$authUser = $this->customer->finduser($user->email);
        if (!$authUser==null) {
            return $authUser;
        }
        else{
        $this->customer->create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id,
            'password' => ''
        ]);
        $authUser = $this->customer->finduser($user->email);
        return $authUser;

}

}
}
    public function logout()
    {
        Auth::guard('customer')->logout();
        return redirect(property_exists($this, 'redirectAfterLogout') ? $this->redirectAfterLogout : '/');
    }

}
