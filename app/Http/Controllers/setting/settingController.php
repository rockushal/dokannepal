<?php

namespace App\Http\Controllers\setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Repository\Setting\SettingRepositoryInterface;

class settingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct(SettingRepositoryInterface $setting)
    {
        $this->middleware('auth');
        $this->setting=$setting;

    }
    public function index()
    {
        $setting=$this->setting->index()->first();
        return view('layouts/admin/pages/setting/setting', compact('setting'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         try{
            $setting=$this->setting->find($id);
        if($request->hasFile('logo'))
            {
                
                if(!$setting->logo == ""){
                unlink(public_path()."/images/logo/".$setting->logo);
            }
                //upload new image
                $logoName = time().'-'.$request->file('logo')->getClientOriginalName();
                $path = 'public/images/logo/';
                $request->file('logo')->move($path,$logoName);
                $settingdetails['logo']=$logoName;
            }
            if($request->hasFile('favicon'))
            {
                if(!$setting->favicon == ""){
                unlink(public_path()."/images/icon/".$setting->favicon);
            }
                $iconName = time().'-'.$request->file('favicon')->getClientOriginalName();
                $path = 'public/images/icon/';
                $request->file('favicon')->move($path,$iconName);
                $settingdetails['favicon']=$iconName;
            }
        $settingdetails['title']=$request->title;
        $settingdetails['email']=$request->email;
        $settingdetails['Address']=$request->address;
        $settingdetails['phone']=$request->phone;
        $settingdetails['mobile']=$request->mobile;
        $settingdetails['facebook_link']=$request->facebook;
        $settingdetails['goole_plus']=$request->google;
        $settingdetails['twitter']=$request->twitter;
        $settingdetails['admin_id']=$request->user_id;
        $settin=$this->setting->update($id,$settingdetails);
        return redirect()->route('setting.index')->with('success', 'Successfully Updated');
    }
    catch(\Exception $e){
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
