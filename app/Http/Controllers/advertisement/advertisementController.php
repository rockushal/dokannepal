<?php 
namespace App\Http\Controllers\advertisement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Repository\Advertisement\AdvertisementRepositoryInterface;

class advertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(AdvertisementRepositoryInterface $advertisement)
    {
        $this->advertisement=$advertisement;
    }
    public function index()
    {
        $advertisement = $this->advertisement->index();
       return view('layouts/admin/pages/advertisement/advertisement',compact('advertisement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts/admin/pages/advertisement/addadvertisement');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->hasFile('img'))
            {
                
                $imgName = time().'-'.$request->file('img')->getClientOriginalName();
                $path = 'public/images/advertisement/';
                $request->file('img')->move($path,$imgName);
                $advdetails['image']=$imgName;
            }
        $advdetails['name']=$request->name;
        $advdetails['description']=$request->description;
        $adv=$this->advertisement->create($advdetails);
        return redirect()->route('advertisement.index')->with('success', 'Your Advertisement has been successfully Added.');
        }
        catch(\Exception $e){
            dd($e);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $editadvertisement=$this->advertisement->find($id);
        return view('layouts.admin.pages.advertisement.editadvertisement',compact('editadvertisement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{

            if($request->hasFile('img'))
        {
            //deleting image
            $advertisement=$this->advertisement->find($id);
            unlink(public_path()."/images/advertisement/".$advertisement->image);
            $imgName = time().'-'.$request->file('img')->getClientOriginalName();
            $path = 'public/images/advertisement/';
            $request->file('img')->move($path,$imgName);
            $advertisementdetails['image']=$imgName;
            }
        $advertisementdetails['name']=$request->name;
        $advertisementdetails['description']=$request->description;
        $user=$this->advertisement->update($id,$advertisementdetails);
        return redirect()->route('advertisement.index')->with('success', 'Your advertisement has been successfully Updated.');
        }
        catch(Exception $e){
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $advertisement=$this->advertisement->find($id);
        // dd($event);
        if(!$advertisement->image == ""){
        unlink(public_path()."/images/advertisement/".$advertisement->image);
    }
        $this->advertisement->delete($id);
        return redirect()->route('advertisement.index')->with('error', 'Your Advertisement has been successfully deleted.');
    }
}
