<?php

namespace App\Http\Controllers\brand;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repo\Repository\Brand\BrandRepositoryInterface;

class brandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct(BrandRepositoryInterface $brand)
    {
        $this->middleware('auth');
        $this->brand=$brand;

    }
    public function index()
    {
        $brand=$this->brand->index();
        return view('layouts/admin/pages/brand/brand', compact('brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('layouts/admin/pages/brand/addbrand');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            if($request->hasFile('img'))
        {
            $imgName = time().'-'.$request->file('img')->getClientOriginalName();
            $path = 'public/images/brand/';
            $request->file('img')->move($path,$imgName);
            $branddetails['logo']=$imgName;
        }
            $branddetails['name']=$request->brandname;
            $branddetails['description']=$request->description;
            $branddetails['slug']=str_slug($branddetails['name'], '-');
            $band=$this->brand->create($branddetails);
            return redirect()->route('brand.index')->with('success', 'Your brand has been successfully Created.');
        }
        catch(Exception $e)
        {
            dd($e);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editbrand=$this->brand->find($id);
        return view('layouts/admin/pages/brand/editbrand',compact('editbrand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            if($request->hasFile('img'))
        {
            //deleting image
            $brand=$this->brand->find($id);
            if(!$brand->logo == ""){
            unlink(public_path()."/images/brand/".$brand->logo);
        }
            //uploading new image
            $imgName = time().'-'.$request->file('img')->getClientOriginalName();
            $path = 'public/images/brand/';
            $request->file('img')->move($path,$imgName);
            $branddetails['logo']=$imgName;
        }
            $branddetails['name']=$request->brandname;
            $branddetails['description']=$request->description;
            $branddetails['slug']=str_slug($branddetails['name'], '-');

            $this->brand->update($id,$branddetails);
            return redirect()->route('brand.index')->with('success', 'Your brand has been successfully Updated.');
        }
        catch(Exception $e){
            dd($e);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
           $brand=$this->brand->find($id);
        // dd($event);
        if(!$brand->logo == ""){
        unlink(public_path()."/images/brand/".$brand->logo);
    }
        $this->brand->delete($id);
        return redirect()->route('brand.index')->with('success', 'Your brand has been successfully deleted.');
    }
    
}
