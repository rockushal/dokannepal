<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class FrontServiceProvider extends ServiceProvider
{
  /**
  * Bootstrap the application services.
  *
  * @return void
  */
  public function boot()
  {
    $this->getSettings();
  }

  /**
  * Register the application services.
  *
  * @return void
  */
  public function register()
  {
    //
  }

  private function getSettings()
  {
    view()->composer('layouts.front.header','App\Repo\Repository\Setting\SettingRepository@getSettings');
    view()->composer('layouts.front.index','App\Repo\Repository\Product\ProductRepository@getProductByNew');
    view()->composer('layouts.front.index','App\Repo\Repository\Product\ProductRepository@getProductByRating');
    view()->composer('layouts.front.index','App\Repo\Repository\Product\ProductRepository@getProductByDiscount');
    view()->composer('layouts.front.index','App\Repo\Repository\Brand\BrandRepository@getBrand');
  }
}
