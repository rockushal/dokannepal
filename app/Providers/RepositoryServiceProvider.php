<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
  /**
  * Bootstrap the application services.
  *
  * @return void
  */
  public function boot()
  {
    //
  }

  /**
  * Register the application services.
  *
  * @return void
  */
  public function register()
  {
    $this->app->bind(
      'App\Repo\Repository\Brand\BrandRepositoryInterface',
      'App\Repo\Repository\Brand\BrandRepository'
    );
    $this->app->bind(
      'App\Repo\Repository\Setting\SettingRepositoryInterface',
      'App\Repo\Repository\Setting\SettingRepository'
    );
    $this->app->bind(
      'App\Repo\Repository\Category\CategoryRepositoryInterface',
      'App\Repo\Repository\Category\CategoryRepository'
    );
    $this->app->bind(
      'App\Repo\Repository\Product\ProductRepositoryInterface',
      'App\Repo\Repository\Product\ProductRepository'
    );
    $this->app->bind(
      'App\Repo\Repository\Admin\AdminRepositoryInterface',
      'App\Repo\Repository\Admin\AdminRepository'
    );
    $this->app->bind(
      'App\Repo\Repository\Advertisement\AdvertisementRepositoryInterface',
      'App\Repo\Repository\Advertisement\AdvertisementRepository'
    );
    $this->app->bind(
      'App\Repo\Repository\Customer\CustomerRepositoryInterface',
      'App\Repo\Repository\Customer\CustomerRepository'
    );
    $this->app->bind(
      'App\Repo\Repository\Cart\CartRepositoryInterface',
      'App\Repo\Repository\Cart\CartRepository'
    );
    $this->app->bind(
      'App\Repo\Repository\Wish\WishRepositoryInterface',
      'App\Repo\Repository\Wish\WishRepository'
    );
  }
}
