<?php namespace App\Repo\Repository\Customer;

use App\Repo\Module\Customer\Customer;
use DB;
use App\Repo\Repository\BaseRepository;
/**
 * Class TaskRepository
 * @package App\Agentcis\Repositories\userCategory
 */
class CustomerRepository extends BaseRepository implements CustomerRepositoryInterface
{
    /**
     * The Guard instance
     *
     * @var Guard
     */
    protected $auth;

    protected $model;
    /**
     * @param Guard  $auth
     * @param userCategory $userCategory
     */
    public function __construct(Customer $Customer)
    {   

        $this->model = $Customer;
      
    }
    public function finduser($email){
       return $this->model->where('email', $email)->first();
    }
}