<?php namespace App\Repo\Repository\Brand;

use App\Repo\Module\brand;
use DB;
use App\Repo\Repository\BaseRepository;
use Illuminate\Contracts\View\View;

/**
 * Class TaskRepository
 * @package App\Agentcis\Repositories\userCategory
 */
class BrandRepository extends BaseRepository implements BrandRepositoryInterface
{

    /**
     * The Guard instance
     *
     * @var Guard
     */
    protected $auth;

    protected $model;
    /**
     * @param Guard  $auth
     * @param userCategory $userCategory
     */
    public function __construct(Brand $Brand)
    {   
        $this->model = $Brand;
      
    }

    public function getBrand(View $view)
    {
        $brand = $this->model->get();
        $view->with('brand',$brand);
    }
}