<?php namespace App\Repo\Repository\Wish;

use App\Repo\Module\Wish;
use DB;
use App\Repo\Repository\BaseRepository;


/**
 * Class TaskRepository
 * @package App\Agentcis\Repositories\userCategory
 */
class WishRepository extends BaseRepository implements WishRepositoryInterface
{

    /**
     * The Guard instance
     *
     * @var Guard
     */
    protected $auth;

    protected $model;
    /**
     * @param Guard  $auth
     * @param userCategory $userCategory
     */
    public function __construct(Wish $Wish)
    {   
        $this->model = $Wish;
      
    }
    public function getWishByUserId($id){
        return $this->model->where('user_id',$id)->get();
    }
    public function getwish($user_id,$product_id){
        return $this->model->where('user_id',$user_id)->where('product_id',$product_id)->first();
    }
}