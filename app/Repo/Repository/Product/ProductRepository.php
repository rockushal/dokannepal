<?php namespace App\Repo\Repository\Product;

use App\Repo\Module\product;
use App\Repo\Module\cart;
use DB;
use App\Repo\Repository\BaseRepository;
use Illuminate\Contracts\View\View;

/**
* Class TaskRepository
* @package App\Agentcis\Repositories\userCategory
*/
class ProductRepository extends BaseRepository implements ProductRepositoryInterface
{

  /**
  * The Guard instance
  *
  * @var Guard
  */
  protected $auth;

  protected $model;
  /**
  * @param Guard  $auth
  * @param userCategory $userCategory
  */
  public function __construct(Product $Product,Cart $Cart)
  {
    $this->model = $Product;
    $this->cart = $Cart;

  }
  public function listproduct(){
    return $this->model->paginate(12);
  }
  public function searchProduct($value){
    return $this->model->where('name','LIKE','%'. $value .'%')->paginate(12);
  }

  public function getbrandproduct($id){
    return $this->model->where('brand_id',$id)->paginate(12);
  }
  public function getcategoryproduct($id){
    return $this->model->where('category_id',$id)->paginate(12);
  }
  public function getProductByNew(View $view)
  {
    $product = $this->model->get();
    $view->with('product',$product);
  }
  public function getProductByRating(View $view)
  {
    $productrate = $this->model->where('rating', '>=', 1)->get();
    $view->with('productrate',$productrate);
  }

  public function getProductByDiscount(View $view)
  {
    $productdiscount = $this->model->where('discount', '!=', null)->get();
    $view->with('productdiscount',$productdiscount);
  }
}
