<?php namespace App\Repo\Repository\Advertisement;

use App\Repo\Module\advertisement;
use DB;
use App\Repo\Repository\BaseRepository;


/**
 * Class TaskRepository
 * @package App\Agentcis\Repositories\userCategory
 */
class AdvertisementRepository extends BaseRepository implements AdvertisementRepositoryInterface
{

    /**
     * The Guard instance
     *
     * @var Guard
     */
    protected $auth;

    protected $model;
    /**
     * @param Guard  $auth
     * @param userCategory $userCategory
     */
    public function __construct(Advertisement $Advertisement)
    {   
        $this->model = $Advertisement;
      
    }
}