<?php namespace App\Repo\Repository\Admin;

use App\Repo\Module\Admin\User;
use DB;
use App\Repo\Repository\BaseRepository;


/**
 * Class TaskRepository
 * @package App\Agentcis\Repositories\userCategory
 */
class AdminRepository extends BaseRepository implements AdminRepositoryInterface
{
    /**
     * The Guard instance
     *
     * @var Guard
     */
    protected $auth;

    protected $model;
    /**
     * @param Guard  $auth
     * @param userCategory $userCategory
     */
    public function __construct(User $User)
    {   

        $this->model = $User;
      
    }
}