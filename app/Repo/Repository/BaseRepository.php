<?php namespace App\Repo\Repository;

use Illuminate\Auth\Guard;
use DB;


/**
 * Class TaskRepository
 * @package App\Agentcis\Repositories\user
 */
class BaseRepository 
{

    /**
     * The Guard instance
     *
     * @var Guard
     */
    protected $auth;

    // protected $model;


    /**
     * @param Guard  $auth
     */
    public function __construct(Guard $auth)
    {
    }

    public function index()
    {
        return $this->model->orderBy('id','DESC')->get();
    }

    /**
     * Create a user
     * @param       $userDetails
     * @return mixed
     */
    public function create($details)
    {
        DB::beginTransaction();
        try {
            $this->model->fill($details)->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return $e->getMessage();           
        }        
    }

    /**
     * Find an entity with given id
     * @param $id
     * @return mixed
     */
    public function find($id)
    {        
        return $this->model->find($id);
    }

    /**
     * Update an entity with given id
     * @param $id
     * @param $details
     * @return mixed
     */
    public function update($id, $details)
    {      
        DB::beginTransaction();
        try {
            
            $data = $this->model->findOrFail($id);
            $data->update($details);
            $data->save();
            DB::commit();
            return true;
        } catch (\Exception $e) {
            
            DB::rollback();
            return false;           
        } 
    }

    /**
     * Delete an entity
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function getIdBySlug($slug)
    {
        return $this->model->select('id')->where('slug',$slug)->first()->id;
    }

    public function findBySlug($slug)
    {
        return $this->model->where('slug',$slug)->first();
    }
}