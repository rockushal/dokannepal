<?php namespace App\Repo\Repository\Cart;

use App\Repo\Module\cart;
use DB;
use App\Repo\Repository\BaseRepository;


/**
 * Class TaskRepository
 * @package App\Agentcis\Repositories\userCategory
 */
class CartRepository extends BaseRepository implements CartRepositoryInterface
{

    /**
     * The Guard instance
     *
     * @var Guard
     */
    protected $auth;

    protected $model;
    /**
     * @param Guard  $auth
     * @param userCategory $userCategory
     */
    public function __construct(Cart $Cart)
    {   
        $this->model = $Cart;
      
    }
    public function getCartByUserId($id){
        return $this->model->where('user_id',$id)->get();
    }
}