<?php namespace App\Repo\Repository\Setting;

use App\Repo\Module\setting;
use DB;
use App\Repo\Repository\BaseRepository;
use Illuminate\Contracts\View\View;

/**
 * Class TaskRepository
 * @package App\Agentcis\Repositories\userCategory
 */
class SettingRepository extends BaseRepository implements SettingRepositoryInterface
{

    /**
     * The Guard instance
     *
     * @var Guard
     */
    protected $auth;

    protected $model;
    /**
     * @param Guard  $auth
     * @param userCategory $userCategory
     */
    public function __construct(Setting $Setting)
    {   
        $this->model = $Setting;
      
    }
    public function getSettings(View $view)
    {
        $settings = $this->model->find(1);
        $view->with('settings',$settings);
    }
}