<?php

namespace App\Repo\Module;

use Illuminate\Database\Eloquent\Model;
use App\Repo\Module\Wish;
use App\Repo\Module\Product;
use App\Repo\Module\Customer\Customer;

class Wish extends Model
{
    protected $table= 'wishlist';
    

    protected $fillable = ['user_id','product_id'];   

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
