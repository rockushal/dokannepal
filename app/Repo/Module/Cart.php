<?php

namespace App\Repo\Module;

use Illuminate\Database\Eloquent\Model;
use App\Repo\Module\Cart;
use App\Repo\Module\Product;
use App\Repo\Module\Customer\Customer;

class Cart extends Model
{
    protected $table= 'cart';
    

    protected $fillable = ['user_id','product_id','no_of_item'];   

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function user()
    {
        return $this->belongsTo(Customer::class,'user_id','id');
    }
}
