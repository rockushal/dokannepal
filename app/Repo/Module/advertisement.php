<?php
namespace App\Repo\Module;

use Illuminate\Database\Eloquent\Model;
use App\Repo\Module\Advertisement;


class Advertisement extends Model
{
    protected $table= 'advertisment';
    

    protected $fillable = ['name','image','description','admin_id'];   
}
