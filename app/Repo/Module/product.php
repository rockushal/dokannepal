<?php

namespace App\Repo\Module;

use Illuminate\Database\Eloquent\Model;
use App\Repo\Module\Category;
use App\Repo\Module\Brand;
class product extends Model
{
    protected $table= 'products';
    

    protected $fillable = ['name','description','slug','image','price','specification','rating','discount','category_id','brand_id'];

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class,'brand_id','id');
    }
}
