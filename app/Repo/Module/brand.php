<?php

namespace App\Repo\Module;

use Illuminate\Database\Eloquent\Model;
use App\Repo\Module\Product;

class Brand extends Model
{
    protected $table= 'brands';
    

    protected $fillable = ['name','description','slug','logo'];

     public function product()
    {
        return $this->hasMany(Product::class,'brand_id','id');
    }
}
