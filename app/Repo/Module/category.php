<?php

namespace App\Repo\Module;

use Illuminate\Database\Eloquent\Model;
use App\Repo\Module\Product;


class Category extends Model
{
    protected $table= 'category';
    

    protected $fillable = ['name','slug'];
 
 public function product()
    {
        return $this->hasMany(Product::class,'category_id','id');
    }
   
}
