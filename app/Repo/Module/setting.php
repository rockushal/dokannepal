<?php

namespace App\Repo\Module;

use Illuminate\Database\Eloquent\Model;
use App\Repo\Module\Setting;


class Setting extends Model
{
    protected $table= 'setting';
    

    protected $fillable = ['title','logo','favicon','email','phone','mobile','Address','facebook_link','twitter','goole_plus','admin_id'];   
}
