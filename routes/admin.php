<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//route for product
Route::resource('product', 'product\productController');
//route for category
Route::resource('category', 'category\categoryController');
//route for brand
Route::resource('brand', 'brand\brandController');
//route for setting
Route::resource('setting', 'setting\settingController');
//route for advertisement
Route::resource('advertisement', 'advertisement\advertisementController');

//route for password change
Route::resource('changepassword', 'Auth\adminController');

//route of order
Route::GET('Order','product\ProductController@order')->name('order');