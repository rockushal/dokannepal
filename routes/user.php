<?php
Route::get('/', 'product\frontProductController@home')->name('main');
Route::GET('login','User\LoginController@showLoginForm')->name('user.login');
Route::GET('userlogout','User\LoginController@logout')->name('user.logout');
Route::POST('login','User\LoginController@login');
/*cart list */
Route::GET('my-cart/{id}','product\frontProductController@cartView')->name('cart');
Route::GET('cartdelete/{id}','product\frontProductController@cartDelete')->name('deletecart');
Route::POST('AddCart','product\frontProductController@addToCart')->name('addcart');
/*Wish list */
Route::POST('AddWish','product\frontProductController@addToWish')->name('addwishlist');
Route::GET('my-wish/{id}','product\frontProductController@wishView')->name('wish');
Route::GET('wishdelete/{id}','product\frontProductController@wishDelete')->name('deletewish');
/*product list */
Route::GET('listproduct','product\frontProductController@showListProduct')->name('listproduct');
Route::GET('productdetail/{slug}','product\frontProductController@showProductDetail')->name('listdetail');
/*brand */
Route::GET('Brand/{slug}','product\frontProductController@showListProductByBrand')->name('brand');
/*category */
Route::GET('Category/{slug}','product\frontProductController@showListProductByCategory')->name('category');

Route::POST('Search/Product','product\frontProductController@showSearchProduct')->name('search');
Route::GET('User/Help','product\frontProductController@showHelp')->name('help');
/*social login */
Route::get('auth/{provider}', 'User\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'User\LoginController@handleProviderCallback');
/*Pages */
Route::GET('Contact','product\frontProductController@showContact')->name('contact');
Route::GET('About','product\frontProductController@showAbout')->name('about');
/*Profile*/
Route::GET('My-Profile','User\userController@showProfile')->name('profile');
Route::GET('My-Profile/Edit-Profile','User\userController@showProfileEdit')->name('editprofile');
Route::GET('My-Profile/Change-Password','User\userController@showpassword')->name('password');
Route::POST('changepassword/{id}','User\userController@changepassword')->name('changepassword');
Route::POST('changeprofile/{id}','User\userController@update')->name('profileedit');

Route::POST('registeruser','User\userController@userRegister')->name('userregister');
?>